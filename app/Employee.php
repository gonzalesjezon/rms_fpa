<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $primaryKey = 'RefId';

    protected $table = 'employees';

    protected $fillable = [

		'LastName',
		'FirstName',
		'MiddleName',
		'EmpStatusRefId',

    ];


}
