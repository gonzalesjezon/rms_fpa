<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Examination extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'examinations';

    protected $fillable = [

		'applicant_id',
		'exam_date',
		'exam_time',
		'exam_location',
		'resched_exam_date',
		'resched_exam_time',
		'exam_status',
		'notify',

    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant','applicant_id');
    }
}
