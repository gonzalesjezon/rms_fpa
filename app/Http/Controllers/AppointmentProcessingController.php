<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\AppointmentProcessing;
use App\AppointmentForm;
use App\Job;
use App\JobOffer;
use App\Assumption;
use App\AppointmentChecklist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class AppointmentProcessingController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Processing Checklist');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $appointments = AppointmentForm::latest()
            ->paginate($perPage);

        return view('appointment-processing.index', [
            'appointments' => $appointments
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $appointment = new AppointmentProcessing();
        $applicant = new Applicant();

        if ($request->applicant_id) {
            $applicant = Applicant::where('id', $request->applicant_id)
                ->first();
            $appointment = AppointmentProcessing::where('applicant_id', $request->applicant_id)
                ->first();
        }

        return view('appointment-processing.create')->with([
            'appointment' => $appointment,
            'applicant' => $applicant,
            'action' => 'AppointmentProcessingController@store',
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(isset($request->appointment_id)){
            $appointment = AppointmentProcessing::find($request->appointment_id);
            $appointment->fill($request->all());
            $appointment->educ_check = ($request->educ_check) ? 1 : 0;
            $appointment->exp_check = ($request->exp_check) ? 1 : 0;
            $appointment->training_check = ($request->training_check) ? 1 : 0;
            $appointment->eligibility_check = ($request->eligibility_check) ? 1 : 0;
            $appointment->other_check = ($request->other_check) ? 1 : 0;
            if($appointment->save()){
                // $checklist = $this->storeAppointee($appointment->applicant_id);
            }

        }

        return redirect('/appointment-processing')->with('success', 'The Appointment was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $appointment = AppointmentProcessing::destroy($id);
        $appointmentform = AppointmentForm::where('applicant_id',$appointment->applicant_id)->delete();
        return redirect('/appointment-processing')->with('success', 'Appointment data deleted!');
    }

    public function report(Request $request){

        $appointment = new AppointmentProcessing();
        $applicant = new Applicant();

        if ($request->applicant_id) {
            $applicant = Applicant::where('id', $request->applicant_id)
                ->first();
            $appointment = AppointmentProcessing::where('applicant_id', $request->applicant_id)
                ->first();
        }

        return view('appointment-processing.report')->with([
            'appointment' => $appointment,
            'applicant' => $applicant,
        ]);
    }

    public function storeAppointee($applicant_id){

        $appointment = AppointmentChecklist::where('applicant_id',$applicant_id)->first();
        if (empty($appointment)) {
            $appointment = new AppointmentChecklist;
        }
        if($appointment->exists()){
            $appointment->updated_by = Auth::id();
            $response = redirect('/interviews')->with('success', 'Applicant appointed was updated successfully.');
        }else{
            $appointment->created_by = Auth::id();

            $response = redirect('/interviews')->with('success', 'Applicant appointed was created successfully.');
        }
        $appointment->applicant_id = $applicant_id;
        $appointment->save();
    }


}
