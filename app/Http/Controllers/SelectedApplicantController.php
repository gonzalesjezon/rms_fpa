<?php

namespace App\Http\Controllers;

use App\SelectionLineup;
use App\PreliminaryEvaluation;
use App\Job;
use App\Applicant;
use App\Examination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
class SelectedApplicantController extends Controller
{
     /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Selection Line Up');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $selected = SelectionLineup::latest()
            ->paginate($perPage);

        return view('selected_applicant.index', [
            'selectedData' => $selected,
            'action' => 'SelectedApplicantController@storeExam'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $jobs = Job::leftJoin('psipop as p','p.id','=','jobs.psipop_id')
            ->where('publish',1)
            ->select('p.position_title as title', 'jobs.id as id')->getModels();

        $recommend = SelectionLineup::select('applicant_id')->get()->toArray();
        $applicants = Applicant::where('qualified',1)->whereNotIn('id',$recommend)->getModels();

        return view('selected_applicant.create')->with([
            'action' => 'SelectedApplicantController@store',
            'actionQualified' => 'SelectedApplicantController@selectedApplicant',
            'jobs' => $jobs,
            'applicants' => $applicants,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        foreach ($request->recommend as $key => $recommendData) {

            if($recommendData['checked']){
                $recommend = new SelectionLineup;
                $recommend->applicant_id = $recommendData['applicant_id'];
                $recommend->created_by = Auth::id();
                $recommend->save();
            }
        }

        return redirect('/selected_applicant')->with('success','Selected applicant was saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SelectedApplicant  $selectedApplicant
     * @return \Illuminate\Http\Response
     */
    public function show(SelectedApplicant $selectedApplicant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SelectedApplicant  $selectedApplicant
     * @return \Illuminate\Http\Response
     */
    public function edit(SelectedApplicant $selectedApplicant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SelectedApplicant  $selectedApplicant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SelectedApplicant $selectedApplicant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SelectedApplicant  $selectedApplicant
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SelectionLineup::destroy($id);
        return redirect('/selected_applicant')->with('success','Selected applicant was deleted successfully.');
    }

    public function selectedApplicant(Request $request){
        $preliminary = new PreliminaryEvaluation;
        $preliminary = $preliminary->select('applicant_id')->get()->toArray();

        $currentJob = new Job();
        $applicant  = new Applicant;
        $recommend  = new SelectionLineup;
        $applicants = [];
        if ($request->position_consideration != 0) {
            $currentJob = Job::find($request->position_consideration);
            $recommend = $recommend->pluck('applicant_id')->toArray();
            $applicants = $applicant
                ->whereNotIn('id',$recommend)
                ->where('job_id',$request->position_consideration)
                ->where('qualified',1)->getModels();
        }

        $jobs = Job::leftJoin('psipop as p','p.id','=','jobs.psipop_id')
            ->select('p.position_title as title', 'jobs.id as id')->getModels();

        return view('selected_applicant.create')->with([
            'action' => 'SelectedApplicantController@store',
            'actionQualified' => 'SelectedApplicantController@selectedApplicant',
            'applicants' => $applicants,
            'currentJob' => $currentJob,
            'jobs' => $jobs,
        ]);
    }

    public function storeExam(Request $request)
    {

        $examination = Examination::where('applicant_id',$request->applicant_id)->first();
        if (empty($examination)) {
            $examination = new Examination;
        }
        if($examination->exists()){
            $examination->updated_by = Auth::id();
            $response = redirect('/selected_applicant')->with('success', 'Applicant appointed was updated successfully.');
        }else{
            $examination->created_by = Auth::id();

            $response = redirect('/selected_applicant')->with('success', 'Applicant appointed was created successfully.');
        }
        $examination->applicant_id = $request->applicant_id;
        $examination->save();

        $recommend = SelectionLineup::find($request->id);
        $recommend->status = 1;
        $recommend->updated_by = Auth::id();
        $recommend->save();

        return $response;
    }
}
