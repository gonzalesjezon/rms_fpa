<?php

namespace App\Http\Controllers;

use App\Job;
use App\PreliminaryEvaluation;
use App\Applicant;
use App\Recommendation;
use App\AppointmentProcessing;
use App\AppointmentForm;
use App\AppointmentIssued;
use App\AppointmentCasual;
use App\SelectionLineup;
use App\ErasureAlteration;
use App\AcceptanceResignation;
use App\Evaluation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use DateTime;

class ReportController extends Controller
{
    /**
     * @var array list of available report documents
     */
    protected $reports = [
        // 'preliminary_evaluation' => 'Preliminary Evaluation',
        // 'selection_lineup' => 'Selection Line Up',
        // 'checklist' => 'Checklist',
        'appointment-transmital' => 'CS Form No. 1 Appointment Transmittal and Action Form',
        'appointments_issued' => 'CS Form No. 2 - Report on Appointment Issued (RAI)',
        // 'absence_qualified_eligible' => 'CS Form No. 5 Certification of the  Absence of a Qualified Eligible',
        'absence_qualified_eligible' => 'CS Form No. 5 Certification that there is no applicant who meets all the qualifications requirements',
        'dibar' => 'CS Form No. 8 Report on DIBAR',
        'publication_vacant_position' => 'CS Form No. 9 Request for Publication of Vacant Positions',
        // 'appointment_form_regulated' => 'CS Form No. 33-A Revised 2018 Appointment Form',
        'appointments_casual' => 'CS Form No. 34-A Plantilla of Casual Appointment Regulated',
        'medical_certificate' => 'CS Form No. 211 Medical Certificate',
        // 'accession_form' => 'Accession Form',
        // 'separation_form' => 'Separation Form',
        // 'oath_office' => 'CS Form No. 32 Oath of Office',
        'checklist' => 'Appointment Processing Checklist',
        'talent_placement_list' => 'Talent Placement List',
        'summary_of_comparative' => 'Summary of Comparative Matrix'
        // 'position_description' => 'DBM-CSC Form No. 1 Position Description Forms',
    ];

    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Reports');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::leftJoin('positionitem as p','p.RefId','=','jobs.plantilla_item_id')
            ->where('publish',1)->getModels();

        $applicants = DB::table('applicants')
            ->where('qualified',1)
            ->select(DB::raw('CONCAT(`first_name`, " " ,`last_name`) as fullname, id'))
            ->orderBy('first_name','asc')
            ->get()->pluck('fullname', 'id')->toArray();

        return view('report.index', [
            'reports' => $this->reports,
            'jobs' => $jobs,
            'applicants' => $applicants,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('report.sched-exam');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function appointmentTransmital(Request $request){
        // from issued sabi ni sir rens
        return view('report.appointment-transmital');
    }

    public function appointmentIssued(Request $request){
        $date = new DateTime($request->date);
        $print_date = $date->format('F Y');

        $appointment = new AppointmentIssued();

        return view('report.appointments_issued')->with([
            'print_date' => $print_date,
            'appointments' => $appointment->getModels()
        ]);
    }

    public function appointmentCasual(Request $request){
        $casual = AppointmentCasual::getModels();
        return view('report.appointments_casual',[
            'casuals' => $casual
        ]);
    }

    public function absenceQualifiedEligible(Request $request){
        return view('report.absence_qualified_eligible');
    }

    public function dibarReport(Request $request){
        return view('report.dibar');
    }

    public function sepearationForm(Request $request){
        return view('report.separation_form');
    }

    public function accessionForm(Request $request){
        return view('report.accession_form');
    }

    public function vacantPosition(Request $request){

        $jobs = Job::where('publication_3',1)->getModels();

        return view('report.publication_vacant_position',[
            'jobs' => $jobs
        ]);
    }

    public function oathOffice(Request $request){

        $applicant = Applicant::find($request->id);

        return view('report.oath_office',[
            'applicant' => $applicant,
        ]);
    }

    public function appointmentFormRegulated(Request $request){
        $applicant = AppointmentForm::where('applicant_id',$request->id)->first();
        $numberInWord = $this->convert_number_to_words(@$applicant->applicant->job->monthly_basic_salary);

        return view('report.appointment_form_regulated',[
            'applicant' => $applicant,
            'number_in_word' => $numberInWord
        ]);
    }

    public function medicalCertificate(Request $request){
        $applicant = Applicant::find($request->id)->first();
        return view('report.medical_certificate',[
            'applicant' => $applicant
        ]);
    }

    public function preliminaryEvaluation(Request $request){

        $applicant = Applicant::where('job_id',$request->id)->pluck('id')->toArray();
        $preliminary = PreliminaryEvaluation::whereIn('applicant_id',$applicant)->getModels();
        $jobs = Job::where('id',$request->id)->first();

        return view('report.preliminary_evaluation')
        ->with([
            'preliminary' => $preliminary,
            'jobs' => $jobs,

        ]);
    }

    public function talentListRepot(Request $request){
        $evaluation = Evaluation::where('job_id',$request->id)->orderBy('total_score','desc')->get();
        $job = Job::find($request->id)->first();
        return view('report.talent_placement_list',[
            'evaluations' => $evaluation,
            'job' => $job
        ]);
    }

    public function selectionLineup(Request $request){

        $applicants = Applicant::where('job_id',$request->id)->pluck('id')->toArray();
        $job = Job::find($request->id);
        $recommend = SelectionLineup::whereIn('applicant_id',$applicants)->getModels();

        return view('report.selection_lineup')
        ->with([
            'recommend' => $recommend,
            'jobs' => $job
        ]);
    }

    public function checklistReport(Request $request){


        $appointment = AppointmentProcessing::where('applicant_id', $request->id)->first();

        if(empty($appointment)){
            return redirect('404/');
        }

        return view('report.checklist')->with([
            'appointment' => $appointment,
        ]);
    }

    public function summaryComparative(Request $request){

        $job = Job::find($request->id)->first();
        $assessments = Evaluation::where('job_id',$request->id)->orderBy('total_score','desc')->getModels();

        return view('report.summary_of_comparative',[
            'job' => $job,
            'assessments' => $assessments
        ]);
    }


}
