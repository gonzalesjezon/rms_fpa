<?php

namespace App\Http\Controllers;

use App\Examination;
use App\Job;
use App\SelectionLineup;
use App\Applicant;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
class ExaminationController extends Controller
{

    public function __construct()
    {
        View::share('title', 'Schedule of Examination');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $examination = Examination::latest()
        ->paginate($perPage);

        return view('examinations.index',[
            'examinations' => $examination,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobs = Job::leftJoin('positionitem as p','p.RefId','=','jobs.plantilla_item_id')
            ->where('publish',1)->getModels();

            return view('examinations.create',[
                'jobs' => $jobs,
                'method' => 'POST',
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $examination =  new Examination();
        $examination->fill($request->all());
        $examination->notify = ($request->notify) ? 1 : 0;
        $examination->confirmed = ($request->confirmed) ? 1 : 0;
        $examination->created_by = Auth::id();
        $examination->save();

        if($request->notify == 1){
            $message['status'] = $request->exam_status;
            $message['data'] = $examination;
            $message['type'] = 'exam';

            $this->mail($request->email, 'Subject', $message);
        }

        return redirect()
            ->route('examinations.edit',[
                'exam_id' => $examination->id,
            ])->with('success', 'The examination schedule was successfully created.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Examination  $examination
     * @return \Illuminate\Http\Response
     */
    public function show(Examination $examination)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Examination  $examination
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $examination = Examination::find($id);
        $applicant = Applicant::find($examination->applicant_id);
        $currentJob = 0;
        if(isset($applicant->job_id)){
            $currentJob = Job::find($applicant->job_id);
        }

        $jobs = Job::leftJoin('positionitem as p','p.RefId','=','jobs.plantilla_item_id')
            ->where('publish',1)->getModels();

        $applicants = Applicant::where('qualified',1)
        ->where('job_id',$applicant->job_id)
        ->getModels();

        return view('examinations.edit',[
            'jobs' => $jobs,
            'currentJob' => $currentJob,
            'qualified' => $applicants,
            'examination' => $examination,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Examination  $examination
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $examination = Examination::find($request->examination_id);
        $examination->fill($request->all());
        $examination->notify = ($request->notify) ? 1 : 0;
        $examination->confirmed = ($request->confirmed) ? 1 : 0;
        $examination->updated_by = Auth::id();
        $examination->save();

        if($examination->notify == 1 && $examination->exam_status != 7){
            $message['status'] = $request->exam_status;
            $message['data'] = $examination;
            $message['type'] = 'exam';

            $this->mail($request->email, 'Subject', $message);
        }

        $jobs = Job::leftJoin('positionitem as p','p.RefId','=','jobs.plantilla_item_id')
            ->where('publish',1)->getModels();

        return redirect()
            ->route('examinations.edit', [
                'exam_id' => $examination->id,
            ])
            ->with('success', 'The examination schedule was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Examination  $examination
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Examination::destroy($id);
        return redirect('examinations')->with('success', 'Examination schedule deleted!');
    }

    public function getApplicant(Request $request){

        $applicants = new Applicant();

        $currentJob = 0;
        if(isset($request->job_id)){
            $currentJob = Job::find($request->job_id);
        }
        $applicants = $applicants
        ->where('qualified',1)
        ->where('job_id',$request->job_id)
        ->getModels();

        $jobs = Job::leftJoin('positionitem as p','p.RefId','=','jobs.plantilla_item_id')
            ->where('publish',1)->getModels();;

        return view('examinations.create',[
            'qualified' => $applicants,
            'jobs' => $jobs,
            'currentJob' => $currentJob,
        ]);

    }

    public function mail($to, $subject, $message){

        Mail::to($to)->send(new SendMailable($message));

    }
}
