<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Attestation;
use App\AppointmentForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class AttestationController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Attestation');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $attestations = Attestation::with([
                'applicant.job' => function ($query) {
                    $query->where('status', '=', 'plantilla');
                }
            ]
        )->paginate($perPage);

        return view('attestation.index', [
            'attestations' => $attestations
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attestation  $attestation
     * @return \Illuminate\Http\Response
     */
    public function show(Attestation $attestation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attestation  $attestation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $attestation = Attestation::find($id);
        $appointmentform = AppointmentForm::where('applicant_id',$attestation->applicant_id)->first();

        return view('attestation.edit')->with([
            'attestation' => $attestation,
            'appointmentform' => $appointmentform
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attestation  $attestation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attestation = Attestation::find($id);
        $attestation->fill($request->all());
        $attestation->updated_by = Auth::id();

        $attestation->save();

       return redirect('/attestation')->with('success', 'Attestation was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attestation  $attestation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attestation $attestation, $id)
    {
        Attestation::destroy($id);
        return redirect('/attestation')->with('success', 'Attestation was successfully deleted.');
    }
}
