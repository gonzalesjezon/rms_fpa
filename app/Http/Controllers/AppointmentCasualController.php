<?php

namespace App\Http\Controllers;

use App\AppointmentCasual;
use App\Applicant;
use App\AppointmentForm;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Auth;

class AppointmentCasualController extends Controller
{
    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Plantilla of Casual Appointment');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $appointments = AppointmentForm::latest()
            ->paginate($perPage);

        return view('appointment-casual.index', [
            'appointments' => $appointments
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $applicant       = new Applicant;
        $appointmentcasual = new AppointmentCasual;
        if (isset($request->applicant_id)) {
            $applicant = $applicant->where('id', $request->applicant_id)
                ->first();
            $appointmentcasual = $appointmentcasual->where('applicant_id',$request->applicant_id)->first();
        }

        return view('appointment-casual.create')->with([
            'applicant' => $applicant,
            'appointmentcasual' => $appointmentcasual,
            'action' => 'AppointmentCasualController@store',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $appointmentcasual = AppointmentCasual::find($request->appointmentcasual_id);
        if(empty($appointmentcasual)){
            $appointmentcasual = new AppointmentCasual;
        }
        $appointmentcasual->fill($request->all());
        if($appointmentcasual->exists()){
            $appointmentcasual->updated_by = Auth::id();
            $response = 'The Appointment was successfully updated.';
        }else{
            $appointmentcasual->created_by = Auth::id();
            $response = 'The Appointment was successfully created.';
        }
        $appointmentcasual->save();
        return redirect('/appointment-casual')->with('success', $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AppointmentCasual::destroy($id);
        return redirect('/appointment-casual')->with('success', 'Appointment data deleted!');
    }
}
