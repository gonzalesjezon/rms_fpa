<?php

namespace App\Http\Controllers;

use App\AppointmentRequirement;
use App\Applicant;
use App\AppointmentForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class AppointmentRequirementController extends Controller
{

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Pre Employment Requirements');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $appointments = AppointmentRequirement::latest()
            ->paginate($perPage);

        return view('appointment-requirements.index', [
            'appointments' => $appointments
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $applicant   = new Applicant;
        $requirement = new AppointmentRequirement;

        if (isset($request->applicant_id)) {
            $applicant = $applicant->where('id', $request->applicant_id)
                ->first();
            $requirement = $requirement->where('applicant_id',$request->applicant_id)->first();
        }

        return view('appointment-requirements.create')->with([
            'applicant' => $applicant,
            'requirement' => $requirement,
            'action' => 'AppointmentRequirementController@store',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requirement = AppointmentRequirement::find($request->requirement_id);
        if(empty($requirement)){
            $requirement = new AppointmentRequirement;
        }
        $requirement->fill($request->all());
        $requirement->saveDocumentFileNames($request);
        if($requirement->exists()){
            $requirement->updated_by = Auth::id();
            $response = 'The Appointment - Requirements was successfully updated.';
        }else{
            $requirement->created_by = Auth::id();
            $response = 'The Appointment - Requirements was successfully created.';
        }

        if($requirement->save()){
             $requirement->uploadDocumentFiles($request);
        }
        return redirect('/appointment-requirements')->with('success', $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppointmentRequirement  $appointmentRequirement
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {

        $requirement = AppointmentRequirement::where('applicant_id',$id)->first();
        return view('appointment-requirements.show', [
            'requirement' => $requirement,
            'documentView' => $request->document
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppointmentRequirement  $appointmentRequirement
     * @return \Illuminate\Http\Response
     */
    public function edit(AppointmentRequirement $appointmentRequirement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppointmentRequirement  $appointmentRequirement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AppointmentRequirement $appointmentRequirement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppointmentRequirement  $appointmentRequirement
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        AppointmentRequirement::where('applicant_id',$id)->delete();
        return redirect('/appointment-requirements')->with('success', 'Appointment - Requirements data deleted!');
    }
}
