<?php

namespace App\Http\Controllers;

use App\OathOffice;
use App\Applicant;
use App\AppointmentForm;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class OathOfficeController extends Controller
{

    public function __construct()
    {
        View::share('title', 'Oath of Office');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $forms = AppointmentForm::latest()->paginate($perPage);

        return view('oath-office.index', [
            'forms' => $forms
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function show(OathOffice $oathOffice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $oathoffice = OathOffice::where('applicant_id',$id)->first();

        if(empty($oathoffice)){
            $oathoffice = AppointmentForm::where('applicant_id',$id)->first();
        }

        return view('oath-office.edit')->with([
            'oathoffice' => $oathoffice,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oathoffice = OathOffice::find($id);
        if(empty($oathoffice)){
            $oathoffice =  new OathOffice;
        }
        $oathoffice->fill($request->all());
        $oathoffice->updated_by = Auth::id();

        $oathoffice->save();

       return redirect('/oath-office')->with('success', 'Oath of Office was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        OathOffice::where('applicant_id',$id)->delete();
        return redirect('/oath-office')->with('success', 'Oath of Office was successfully deleted.');
    }

    public function oathOfficeReport(Request $request){

        $oathoffice = OathOffice::where('applicant_id',$request->id)->first();

        return view('oath-office.report',[
            'oathoffice' => $oathoffice,
        ]);
    }
}
