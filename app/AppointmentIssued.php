<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentIssued extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'appointment_issued';
    protected $fillable = [

		'applicant_id',
		'employee_status',
		'date_issued',
		'period_of_employment_from',
		'period_of_employment_to',
		'nature_of_appointment',
		'appointing_officer',
		'publication_date_from',
		'publication_date_to',
		'hrmo',
		'date_sign',
		'chairperson',
		'chairperson_date_sign',
		'hrmo_date_sign',
		'deliberation_date',
		'assessment_date',

    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
