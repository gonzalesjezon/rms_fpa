<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $primaryKey = 'RefId';

    protected $table = 'department';

    protected $fillable = [
    	'Code',
    	'Name'
    ];

    public function psipop(){
    	return $this->belongsTo('App\PSIPOP');
    }
}
