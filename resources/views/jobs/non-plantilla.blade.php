@extends('layouts.app')

@section('css')
  <link rel="stylesheet"
        type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" />
@endsection

@section('content')
  <div class="page-head">
    <h2 class="page-head-title">Non-Plantilla</h2>
  </div>
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-table">
          <div class="card-header">
            <a href="{{ route('jobs.create',['status'=>'non-plantilla']) }}"
               class="btn btn-space btn-primary"
               title="Add a vacant position">
              <i class="icon icon-left mdi mdi-account-add"> </i> Add
            </a>
            <a href="{{ route('jobs.create',['status'=>'non-plantilla']) }}"
               class="btn btn-space btn-warning"
               title="Add a vacant position">
              <i class="icon icon-left mdi mdi-account-add"> </i> Print
            </a>
            <div class="tools dropdown">
              <span class="icon mdi mdi-download"> </span>
              <a href="#" role="button" data-toggle="dropdown" class="dropdown-toggle">
                <span class="icon mdi mdi-more-vert"> </span>
              </a>
              <div role="menu" class="dropdown-menu">
                <a href="#" class="dropdown-item">Action</a>
                <a href="#" class="dropdown-item">Another action</a>
                <a href="#" class="dropdown-item">Something else here</a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">Separated link</a>
              </div>
            </div>
          </div>
          <div class="card-body">
            <table id="table1" class="table table-striped table-hover table-fw-widget">
              <thead>
              <tr>
                <th>Actions</th>
                <th>Daily Salary</th>
                <th>Position Title</th>
                <th>Office/Division</th>
                <th>Publish</th>
                <th>Date Posted</th>
              </tr>
              </thead>
              <tbody>
              @foreach($jobs as $job)
                <tr>
                  <td class="actions text-left">
                    <div class="tools">
                      <button type="button"
                              data-toggle="dropdown"
                              class="btn btn-secondary dropdown-toggle"
                              aria-expanded="false">
                        <i class="icon icon-left mdi mdi-settings-square"> </i> Options
                        <span class="icon-dropdown mdi mdi-chevron-down"></span>
                      </button>
                      <div role="menu"
                           class="dropdown-menu"
                           x-placement="bottom-start"
                           style="position: absolute; transform: translate3d(-95px, 30px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <a href="{{ route('jobs.show',['id'=>$job->id] ) }}"
                           class="dropdown-item"><i class="icon icon-left mdi mdi-eye"></i>View</a>
                        <div class="dropdown-divider"></div>

                        <a href="{{ route('jobs.edit', ['id' => $job->id, 'status' => $job->status]) }}"
                           class="dropdown-item">
                          <i class="icon icon-left mdi mdi-edit"></i>Edit</a>
                        <div class="dropdown-divider"></div>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['jobs', $job->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<i class="icon icon-left mdi mdi-delete mr-1"></i> Delete', array(
                            'type' => 'submit',
                            'style' => 'color: #504e4e',
                            'class' => 'dropdown-item',
                            'title' => 'Delete Job Post',
                            'onclick'=>'return confirm("Confirm delete?")'
                        ))!!}
                        {!! Form::close() !!}
                      </div>
                    </div>
                  </td>
                  <td>{{ number_format($job->daily_salary, 2) }}</td>
                  <td>{{ $job->title }}</td>
                  <td class="center">{{ $division[$job->division] }}</td>
                  <td>
                    <div class="col-12 col-sm-8 col-lg-6 pt-1">
                      <div class="switch-button switch-button-success switch-button-yesno">
                        <input
                          type="checkbox"
                          name="publish-{{$job->id}}"
                          class="publish-link"
                          id="swt{{$job->id}}"
                          data-id="{{$job->id}}"
                          {{ $job->publish ? 'checked' : '' }}
                        >
                        <span><label for="swt{{$job->id}}"></label></span>
                      </div>
                    </div>
                  </td>
                  <td class="center">{{ $job->created_at->diffForHumans() }}
                    / {{ $job->created_at->format('Y-m-d')}}</td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net/js/jquery.dataTables.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.dataTables();

      $('.publish-link').change(function() {
        var job = $(this);
        let jobID = job.data('id');

        $.ajax({
          url: 'jobs/publish?id=' + jobID,
          type: 'get',
          data: {
            _method: 'get',
            data: {
              '_token': '{{ csrf_token() }}',
            },
          },
          success: function(result) {
            if (result) {
              //job.attr('checked', 'checked');
            }
          },
        });
      });
    });
  </script>
@endsection
