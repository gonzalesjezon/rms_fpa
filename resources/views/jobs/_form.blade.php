@section('css')
<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection

{!! Form::open(['action' => $actionPosition, 'method' => 'GET', 'id' => 'get-form']) !!}
<input type="hidden" name="request_status" value="{{ $status }}">
<input type="hidden" name="job_id" value="{{@$job->id}}">
<div class="row">
    <div class="col-6">

        <div class="form-group row {{ $errors->has('plantilla_item_id') ? 'has-error' : ''}}">
            {{ Form::label('plantilla_item_id', 'Plantilla Item No', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('plantilla_item_id', $plantilla_item, @$currentItem->RefId, [
                    'id' => 'plantilla_item_id',
                    'class' => 'form-control form-control-xs',
                    'placeholder' => 'Select position'
                ])
                }}
                {!! $errors->first('plantilla_item_id', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Position Title', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', @$currentItem->position->Name, [
                    'class' => 'form-control form-control-sm',
                    'readOnly' => true,
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('employee_status', 'Employee Status', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('employee_status_id', config('params.employee_status'), @$job->employee_status_id, [
                    'id'          => 'employee_status_id',
                    'class'       => 'form-control form-control-xs',
                    'placeholder' => 'Select employee status'
                    ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Office', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', @$currentItem->office->Name,[
                        'class' => 'form-control form-control-xs',
                        'readOnly' => true

                    ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Division', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', @$currentItem->division->name,[
                        'class' => 'form-control form-control-xs',
                        'readOnly' => true,
                    ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Place of Assignment', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', @$job->place_of_assignment,[
                        'class' => 'form-control form-control-sm',
                        'id' => 'get_assignment'
                    ])
                }}
            </div>
        </div>
    </div>

    <div class="col-6">
        @if($status === 'plantilla')
        <div class="form-group row">
            {{ Form::label('', 'Salary Grade', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', @$currentItem->salary_grade->Name, [
                    'class' => 'form-control form-control-sm',
                    'readOnly' => true,
                ])
                }}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('monthly_basic_salary') ? 'has-error' : ''}}">
            {{ Form::label('monthly_basic_salary', 'Monthly Basic Salary', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-xl-6 col-sm-8 col-lg-6">
                <input type="text" name="monthly_basic" id="monthly_basic" value="{{ number_format(@$currentItem->SalaryAmount,2) }}" class="form-control form-control-sm" placeholder="PHP 0">
                {!! $errors->first('monthly_basic_salary', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('annual_basic_salary') ? 'has-error' : ''}}">
            {{ Form::label('annual_basic_salary', 'Annual Basic Salary', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-xl-6 col-sm-8 col-lg-6">
                {{ Form::text('', number_format(@$currentItem->SalaryAmount * 12,2), [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'PHP 0',
                    'readOnly' => true,
                ])
                }}
                {!! $errors->first('annual_basic_salary', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
        @endif

        @if($status == 'non-plantilla')
        <div class="form-group row {{ $errors->has('daily_salary') ? 'has-error' : ''}} {{ ($status == 'plantilla') ? 'd-none' : '' }}">
            {{ Form::label('daily_salary', 'Daily Salary', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('daily_salary', @$job->daily_salary, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Daily Salary',
                    'required' => false,
                ])
                }}
                {!! $errors->first('daily_salary', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
         @endif

         <!-- <div class="form-group row {{ $errors->has('reporting_line') ? 'has-error' : ''}}">
            {{ Form::label('reportling_line', 'Reporting Line', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-xl-6 col-sm-8 col-lg-6">
                <input type="text" name="reporting" id="reporting" value="{{@$job->reporting_line}}" class="form-control form-control-sm" >
                {!! $errors->first('reporting_line', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('station') ? 'has-error' : ''}}">
            {{ Form::label('station', 'Station', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-xl-6 col-sm-8 col-lg-6">
                <input type="text" name="station" id="station_line" value="{{@$job->station}}" class="form-control form-control-sm">
                {!! $errors->first('station', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div> -->

    </div>
</div>

{!! Form::close() !!}

<hr>

{!! Form::open(['action' => $action, 'method' => $method ,'id' => 'create-form']) !!}

<input type="hidden" name="plantilla_item_id" value="{{ @$currentItem->RefId }}">
<input type="hidden" name="status" value="{{ $status }}">
<input type="hidden" name="employee_status_id" id="employee_status" value="{{ @$job->employee_status_id }}">
<input type="hidden" name="place_of_assignment" id="place_of_assignment" value="{{ @$job->place_of_assignment }}">
<div class="row">
    <div class="col-6">

        <div class="form-group row ">
            {{ Form::label('', 'Additional Allowance', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right font-weight-bold']) }}
        </div>

        <div class="form-group row {{ $errors->has('pera_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'PERA', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('pera_amount', number_format(2000,2),[
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    'placeholder' => 'PHP 0'
                ]) }}
            </div>
            {!! $errors->first('pera_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

        <div class="form-group row {{ $errors->has('clothing_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'Clothing Allowance', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('clothing_amount', number_format(6000,2),[
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    'placeholder' => 'PHP 0'
                ]) }}
            </div>
            {!! $errors->first('clothing_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

        <div class="form-group row {{ $errors->has('midyear_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'Mid-Year Bonus', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('midyear_amount', number_format(@$currentItem->SalaryAmount * 12 ,2),[
                    'class' => 'form-control form-control-sm',
                    'required' => true,
                    'placeholder' => 'PHP 0',
                    'readonly' => 'true'
                ]) }}
            </div>
            {!! $errors->first('midyear_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

    </div>


    <div class="col-6">

        <div class="form-group row {{ $errors->has('yearend_amount') ? 'has-error' : '' }} mt-8">
            {{ Form::label('', 'Year-End Bonus', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('yearend_amount', number_format(@$currentItem->SalaryAmount * 12 ,2),[
                    'class' => 'form-control form-control-sm',
                    'required' => true,
                    'placeholder' => 'PHP 0',
                    'readonly' => 'true'
                ]) }}
            </div>
            {!! $errors->first('yearend_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>

        <div class="form-group row {{ $errors->has('cashgift_amount') ? 'has-error' : '' }}">
            {{ Form::label('', 'Cash Gift', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('cashgift_amount', @$job->cashgift_amount,[
                    'class' => 'form-control form-control-sm',
                    'required' => true,
                    'placeholder' => 'PHP 0'
                ]) }}
            </div>
            {!! $errors->first('cashgift_amount', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>
    </div>
</div>

<hr>

<div class="form-group row ">
    {{ Form::label('', 'Competency:', ['class' => 'col-12 col-sm-1 col-form-label text-sm-right font-weight-bold']) }}
</div>


<div class="row ">
    <div class="col-6">
        <div class="form-group row {{ $errors->has('compentency_1') ? 'has-error' : ''}}">
            {{ Form::label('compentency_1', 'Core Competency', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-9">
                <div id="compentency_1" name="compentency_1"></div>
                <textarea name="compentency_1" class="d-none"></textarea>
                <input type="hidden" name="compentency_1" id="compentency_1-text">
                {!! $errors->first('compentency_1', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row {{ $errors->has('compentency_2') ? 'has-error' : ''}}">
            {{ Form::label('compentency_2', 'Functional Competency', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="compentency_2" name="compentency_2"></div>
                <textarea name="compentency_2" class="d-none"></textarea>
                <input type="hidden" name="compentency_2" id="compentency_2-text">
                {!! $errors->first('compentency_2', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-6">
        <div class="form-group row {{ $errors->has('compentency_3') ? 'has-error' : ''}}">
            {{ Form::label('', ($status == 'plantilla') ? 'Leadership Competency' : 'Job Summary', ['class'=>'col-32 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="compentency_3" name="compentency_3"></div>
                {{ Form::textarea('compentency_3', '',['id'=>'compentency_3-text', 'class'=>'d-none']) }}
                {!! $errors->first('compentency_3', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

    </div>

    <!-- <div class="col-6">
        <div class="form-group row {{ $errors->has('compentency_4') ? 'has-error' : ''}}"  >
            <span class="col-12 col-sm-4 col-form-label text-sm-right"> {{ ($status == 'plantilla') ? 'Specific Duties & Responsibilities' : 'Auxilliary Functions' }} </span>
            <div class="col-8">
                <div id="compentency_4" name="compentency_4"></div>
                {{ Form::textarea('compentency_4', '',['id'=>'compentency_4-text', 'class'=>'d-none']) }}
                {!! $errors->first('compentency_4', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div> -->
</div>

<div class="row {{ $errors->has('compentency_5') ? 'has-error' : ''}} {{ ($status == 'plantilla') ? 'd-none' : '' }}"">
    <div class="col-6">
        <div class="form-group row {{ $errors->has('compentency_5') ? 'has-error' : ''}}">
            {{ Form::label('', 'Secondary Functions', ['class'=>'col-12 col-sm-5 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="compentency_5" name="compentency_5"></div>
                {{ Form::textarea('compentency_5', '',['id'=>'compentency_5-text', 'class'=>'d-none']) }}
                {!! $errors->first('compentency_5', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<hr>

<div class="form-group row ">
    {{ Form::label('', 'Required Qualifications:', ['class' => 'col-12 col-sm-1 col-form-label text-sm-right font-weight-bold']) }}
</div>

<div class="row">
    <div class="col-6">
        <div class="form-group row {{ $errors->has('education') ? 'has-error' : ''}}">
            {{ Form::label('education', 'Education', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="education" name="education"></div>
                {{ Form::textarea('education', @$job->education,['id'=>'education-text', 'class'=>'d-none']) }}
                {!! $errors->first('education', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('training') ? 'has-error' : ''}}">
            {{ Form::label('training', 'Training', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="training" name="training"></div>
                {{ Form::textarea('training', @$job->training,['id'=>'training-text', 'class'=>'d-none']) }}
                {!! $errors->first('training', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row {{ $errors->has('experience') ? 'has-error' : ''}}">
            {{ Form::label('experience', 'Experience', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="experience" name="experience"></div>
                {{ Form::textarea('experience', @$job->experience,['id'=>'experience-text', 'class'=>'d-none']) }}
                {!! $errors->first('experience', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('eligibility') ? 'has-error' : ''}}">
            {{ Form::label('eligibility', 'Eligibility', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div id="eligibility" name="eligibility"></div>
                {{ Form::textarea('eligibility', @$job->eligibility,['id'=>'eligibility-text', 'class'=>'d-none']) }}
                {!! $errors->first('eligibility', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <div class="form-group row">
            <label class="col-12 col-sm-4 col-form-label text-sm-right">Publish</label>
            <div class="col-12 col-sm-8 col-lg-6 pt-1">
                <div class="switch-button switch-button-success switch-button-yesno">
                    <input type="checkbox" name="publish" id="swt8" {{ @$job->publish ? 'checked' : '' }}><span>
                                        <label for="swt8"></label></span>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-12 col-sm-4 col-form-label text-sm-right"> Deadline Date </label>
            <div class="col-12 col-sm-6">
                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                    <input size="16" type="text" value="{{ @$job->deadline_date }}" name="deadline_date"
                           class="form-control form-control-sm">
                    <div class="input-group-append">
                        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-6">

        <div class="form-group row">
            {{ Form::label('', 'Publications', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-8">
                <div class="row">
                    <label class="col-12 col-sm-4 col-form-label text-sm-right">Agency Website</label>
                    <div class="col-12 col-sm-8 col-lg-6 pt-1">
                        <div class="switch-button switch-button-success switch-button-yesno">
                            <input type="checkbox" name="publication_1" id="swt9" {{ @$job->publication_1 ? 'checked' : '' }}><span>
                                                <label for="swt9"></label></span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-12 col-sm-4 col-form-label text-sm-right">Newspaper</label>
                    <div class="col-12 col-sm-8 col-lg-6 pt-1">
                        <div class="switch-button switch-button-success switch-button-yesno">
                            <input type="checkbox" name="publication_2" id="swt10" {{ @$job->publication_2 ? 'checked' : '' }}><span>
                                                <label for="swt10"></label></span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-12 col-sm-4 col-form-label text-sm-right">CSC Bulletin</label>
                    <div class="col-12 col-sm-8 col-lg-6 pt-1">
                        <div class="switch-button switch-button-success switch-button-yesno">
                            <input type="checkbox" name="publication_3" id="swt11" {{ @$job->publication_3 ? 'checked' : '' }}><span>
                                                <label for="swt11"></label></span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-12 col-sm-4 col-form-label text-sm-right">Other</label>
                    <div class="col-12 col-sm-8 col-lg-6 pt-1">
                        <div class="switch-button switch-button-success switch-button-yesno">
                            <input type="checkbox" name="publication_4" id="swt12" {{ @$job->publication_4 ? 'checked' : '' }}><span>
                                                <label for="swt12"></label></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

<hr>
<input type="hidden" name="reporting_line" value="{{@$job->reporting_line}}" id="reporting_line">
<input type="hidden" name="station" value="{{@$job->station}}" id="station">
<input type="hidden" name="monthly_basic_salary" value="{{ number_format(@$currentItem->SalaryAmount ,2) }}" id="monthly_basic_salary">
<div class="form-group row text-right">
    <div class="col col-sm-12 ">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
{!! Form::close() !!}

@section('scripts')
    @include('jobs._form-script')
@endsection
