@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Edit job post</h2>
    </div>

    <!-- Job Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can add a new vacant position in the form below.</span>
                </div>
                <div class="card-body">
                    @include('jobs._form', [
                        'action' => ['JobsController@update', $job->id],
                        'actionPosition' => 'JobsController@getPsipop',
                        'method' => 'PATCH',
                        'job' => @$job,
                        'plantilla_item' => $plantilla_item,
                        'currentItem' => @$currentItem,
                        'status' => $status
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
