<!-- Left Sidebar -->
<div class="be-left-sidebar">
  <div class="left-sidebar-wrapper">
    <a href="#" class="left-sidebar-toggle">Dashboard</a>
    <div class="left-sidebar-spacer">
      <div class="left-sidebar-scroll">
        <div class="left-sidebar-content">
          <ul class="sidebar-elements">
            <li class="divider">Menu</li>
            <li class="active">
              <a href="{{ route('dashboard') }}"><i class="icon mdi mdi-home"> </i><span>Dashboard</span></a>
            </li>
            <li><a href="{{ route('psipop.index') }}"><i class="icon mdi mdi-collection-item"> </i>
                <span>Itemization & Plantilla</span></a></li>
            <li class="parent">
              <a href="#"><i class="icon mdi mdi-stackoverflow"> </i><span>Job Posting</span></a>
              <ul class="sub-menu">
                <li><a href="{{ route('jobs.index') }}">Plantilla</a></li>
                <li><a href="{{ route('jobs.nonplantilla') }}">Non-Plantilla</a></li>
              </ul>
            </li>

            <li class="parent">
              <a href="#"><i class="icon mdi mdi-face"> </i><span>Applicants</span></a>
              <ul class="sub-menu">
                <li><a href="{{ route('applicant.index') }}">List applicants</a></li>
              </ul>
            </li>

            <li class="parent">
              <a href="#"><i class="icon mdi mdi-star-circle"> </i><span>Evaluation</span></a>
              <ul class="sub-menu">
                <!-- <li><a href="{{ route('preliminary_evaluation.index') }}">Preliminary Evaluation</a></li>
                <li><a href="{{ route('selected_applicant.index') }}">Selection Line Up</a></li> -->
                <li><a href="{{ route('examinations.index') }}">Examination</a></li>
                <li><a href="{{ route('interviews.index') }}">Interview</a></li>
                <li><a href="{{ route('evaluation.index') }}">Individual Assesment</a></li>
                <li><a href="{{ route('comparative-ranking.index')}}">Comparative Ranking</a></li>
              </ul>
            </li>

            <li class="parent">
              <a href="#"><i class="icon mdi mdi-calendar"> </i><span>Appointment</span></a>
              <ul class="sub-menu">
                <li><a href="{{ route('appointment-form.index') }}">Appointment Form</a></li>
                <li><a href="{{ route('appointment-issued.index') }}">Issued</a></li>
                <li><a href="{{ route('appointment-processing.index') }}">Processing Checklist</a></li>
              </ul>
            </li>

            <li><a href="{{ route('appointment-requirements.index') }}">
              <i class="icon mdi mdi-assignment-check"> </i><span>Pre Emp. Requirements</span>
            </a></li>

            <li class="parent">
              <a href="#"><i class="icon mdi mdi-calendar"> </i><span>Others</span></a>
              <ul class="sub-menu">
                 <li><a href="{{ route('appointment-casual.index') }}">Plantilla of Casual Appointment</a></li>
                <li><a href="{{ route('position-descriptions.index')}}">Position Description</a></li>
                <li><a href="{{ route('oath-office.index') }}">Oath of Office</a></li>
                <li><a href="{{ route('erasure_alterations.index') }}">Erasures & Alteration</a></li>
                <li><a href="{{ route('acceptance_resignation.index')}}">Acceptance of Resignation</a></li>
                <li><a href="{{ route('assumption.index') }}"><span>Assumption to Duty</span></a></li>
              </ul>
            </li>

<!--             <li><a href="{{ route('recommendation.index') }}"><i class="icon mdi mdi-assignment-account"> </i>
                <span>Recommendation</span></a></li>
 -->


<!--             <li class="parent">
              <a href="#"><i class="icon mdi mdi-badge-check"> </i><span>Job Offer</span></a>
              <ul class="sub-menu">
                <li><a href="{{ route('joboffer.index') }}">Plantilla</a></li>
                <li><a href="{{ route('joboffer.index', ['status'=>'non-plantilla']) }}">Non-Plantilla</a></li>
              </ul>
            </li>

            <li><a href="{{ route('attestation.index') }}"><i class="icon mdi mdi-assignment"> </i>
                <span>Transmittal for Attestation</span></a></li>
 -->
            <li><a href="{{ route('boarding_applicant.index')}}"><i class="icon mdi mdi-face"> </i>
                <span>Applicant Onboarding</span></a></li>
            <li><a href="{{ route('report.index') }}"><i class="icon mdi mdi-assignment-o"> </i>
                <span>Reports</span></a></li>
            @if(Auth::id() == config('params._SUPER_ADMIN_ID_'))
              <li class="parent">
                <a href="#"><i class="icon mdi mdi-settings"> </i><span>Configurations</span></a>
                <ul class="sub-menu">
                  <li><a href="{{ route('config.index') }}">List configurations</a></li>
                </ul>
              </li>
              <li class="divider">Features</li>
              <li class="parent">
                <a href="#"><i class="icon mdi mdi-inbox"> </i><span>Public Pages</span></a>
                <ul class="sub-menu">
                  <li><a href="{{ route('menu') }}">Menu</a></li>
                  <li><a href="{{ route('careers') }}">Careers</a></li>
                </ul>
              </li>
            @endif
          </ul>
        </div>
      </div>
    </div>

    <div class="progress-widget">
      <div class="progress-data"><span class="progress-value">70%</span><span class="name">Current Project</span>
      </div>
      <div class="progress">
        <div style="width: 60%;" class="progress-bar progress-bar-primary"></div>
      </div>
    </div>
  </div>
</div>
<!-- /. Left Sidebar -->
