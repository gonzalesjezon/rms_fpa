@section('css')
<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection




<div class="row">
    <div class="col-6">
        <div class="form-group row">
            {{ Form::label('', 'Plantilla Item No', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('position_title', @$plantilla_item->Name, [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                ])
                }}
            </div>
        </div>


        <div class="form-group row">
            {{ Form::label('position_title', 'Position Title', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', @$plantilla_item->position->Name, [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('office_id', 'Office', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', @$plantilla_item->office->Name, [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('division_id', 'Division', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', @$plantilla_item->division->Name, [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    ])
                }}
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row {{ $errors->has('department_id') ? 'has-error' : ''}}">
            {{ Form::label('department_id', 'Department', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', @$plantilla_item->department->Name, [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                    ])
                }}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('ppa_attribution') ? 'has-error' : ''}}">
            {{ Form::label('ppa_attribution', 'PPA Attribution', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('ppa_attribution', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                ])
                }}
            </div>
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-6">
        <div class="form-group row">
            {{ Form::label('salary_grade', 'Salary Grade', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('salary_grade', @$plantilla_item->salary_grade->Name, [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('step', 'Step', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('step', @$plantilla_item->step_increment->Name, [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                ])
                }}
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row">
            {{ Form::label('annual_authorized_salary', 'Annual Authorized Salary', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('annual_authorized_salary', '', [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                ]) }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('annual_actual_salary', 'Annual Actual Salary', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', number_format(@$plantilla_item->SalaryAmount * 12,2) , [
                    'class' => 'form-control form-control-sm',
                    'readonly' => true,
                ]) }}
            </div>
        </div>
    </div>
</div>

<!-- <hr> -->
<!--
<div class="row">
    <div class="col-6">
        <div class="form-group row">
            {{ Form::label('', 'Name of Incumbent', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readOnly' => true
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Sex', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readOnly' => true
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Date of Birth', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readOnly' => true
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'TIN', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('','', [
                    'class' => 'form-control form-control-sm',
                    'readOnly' => true
                ])
                }}
            </div>
        </div>
    </div>

    <div class="col-6">
        <div class="form-group row">
            {{ Form::label('', 'Date of Original Appointment', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readOnly' => true
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Date of Last Promotion', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readOnly' => true
                ])
                }}
            </div>
        </div>
    </div>
</div> -->


@section('scripts')
    @include('psipop._form-script')
@endsection
