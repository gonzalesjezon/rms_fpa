@extends('layouts.print')

@section('css')
@endsection

@section('content')
<div class="reports" style="width: 760px;margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">

	<div class="row mb-6 border border-top-1 border-bottom-1 border-left-0 border-right-0 border-dark">
		<div class="col-12 text-center">
			<h4 class="font-weight-bold">METROPOLITAN WATERWORKS & SEWERAGE SYSTEM - REGULATORY OFFICE</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-3">Place of Assignment</div>
		<div class="col-1">:</div>
		<div class="col-3">{!! $jobs->place_of_assignment !!}</div>
	</div>

	<div class="row">
		<div class="col-3">Position Title</div>
		<div class="col-1">:</div>
		<div class="col-6">{{ $jobs->psipop->position_title }}</div>
	</div>

	<div class="row">
		<div class="col-3">Plantilla Item Number</div>
		<div class="col-1">:</div>
		<div class="col-3">{!! $jobs->psipop->item_number !!}</div>
	</div>

	<div class="row">
		<div class="col-3">Salary/Job Grade</div>
		<div class="col-1">:</div>
		<div class="col-3">{{ $jobs->psipop->salary_grade }}</div>
	</div>

	<div class="row">
		<div class="col-3">Monthly Salary</div>
		<div class="col-1">:</div>
		<div class="col-3">{{ number_format($jobs->monthly_basic_salary,2) }}</div>
	</div>

	<div class="row">
		<div class="col-3">Eligibility</div>
		<div class="col-1">:</div>
		<div class="col-3">{!! $jobs->eligibility !!}</div>
	</div>

	<div class="row">
		<div class="col-3">Education</div>
		<div class="col-1">:</div>
		<div class="col-3">{!! $jobs->education !!}</div>
	</div>

	<div class="row">
		<div class="col-3">Training</div>
		<div class="col-1">:</div>
		<div class="col-3">{!! $jobs->training !!}</div>
	</div>

	<div class="row">
		<div class="col-3">Experience</div>
		<div class="col-1">:</div>
		<div class="col-3">{!! $jobs->experience !!}</div>
	</div>

	<div class="row">
		<div class="col-12 font-weight-bold">Compentency: </div>
	</div>

	<div class="row">
		<div class="col-12 font-weight-bold">Instruction/ Remarks</div>
	</div>

	<div class="row">
		<div class="col-10">
			<p>Interested and qualified applicants should signify their interest in writing. Attach the following documents to the application letter and send to the address below not later than </p>
		</div>
	</div>

	<div class="row">
		<div class="col-12 font-weight-bold">Documents:</div>
	</div>

	<div class="row">
		<div class="col-10">
			<ol>
				<li>Fully accomplished Personal Data Sheet (PDS) with reent passport-sized picture (CS Form. 212, Revised 2017) which can be downloaded at www.csc.gov.ph;</li>
				<li>Performance rating in the present position for one (1) year (if applicable);</li>
				<li>Photocopy of certificate of eligibility/rating/license; and</li>
				<li>Photocopy of Transcipt of Records.</li>
			</ol>
		</div>
	</div>

	<div class="row mb-1">
		<div class="col-12"><b>QUALIFIED APPLICANTS</b> are advised to hand in or send through courier/email their application to:</div>
	</div>

	<div class="row mb-1">
		<div class="col-12">
			<p class="font-weight-bold m-0 p-0">VIRGINIA V. OCTA</p>
			<p class="m-0 p-0">Department Manager for Administration</p>
			<p class="m-0 p-0">Metropolitan Waterworks and Sewarage System Regulatory Office</p>
			<p class="m-0 p-0">3rd Floor Engineering Bldg., MWSS Complex, Katipunan Road, Balara, Quezon City</p>
			<p class="m-0 p-0">personnel@ro.mwss.gov.ph</p>
		</div>
	</div>

	<div class="row">
		<div class="col-3">
			<p class="font-weight-bold">Posting Date</p>
		</div>
		<div class="col-1">:</div>
		<div class="col-3">
			<p>{{ date('F d, Y',strtotime($jobs->created_at))}}</p>
		</div>
	</div>

	<div class="row">
		<div class="col-3">
			<p class="font-weight-bold">Closing Date</p>
		</div>
		<div class="col-1">:</div>
		<div class="col-3">
			<p>{{ date('F d, Y',strtotime($jobs->deadline_date))}}</p>
		</div>
	</div>

</div>


@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection