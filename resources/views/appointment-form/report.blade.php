@extends('layouts.print')

@section('css')
@endsection

@section('content')

@if($applicant)
<div id="reports" style="width: 960px;margin: auto; font-size: 10pt;font-family: Arial, Helvetica, sans-serif;">
    <div class="report1" style="border: 15px solid #b1acac;padding: 20px;">
      <div class="row mb-1">
        <div class="col-sm-3 font-weight-bold">CS Form No. 33-B <br> Revised 2018</div>
        <div class="col-sm-6"></div>
        <div class="col-sm-3 text-right"><i style="font-size: 10px;">(Stamp of Date of Receipt)</i></div>
      </div>

      <div class="row mb-6">
        <div class="col-12 text-center">
            <div style="font-size: 16px;" class="font-weight-bold">Republic of the Philippines</div>
            <div style="font-size: 14px;">FERTILIZER AND PESTICIDE AUTHORITY</div>
            <div>FPA Bldg, B.A.I Compound Visayas Ave,</div>
            <div>Diliman, Quezon City, Metro Manila</div>
        </div>
      </div>

      <div class="row mb-4">
        <div class="col-12">
            <p class="font-weight-bold">Mr./Mrs./ Ms.: {!! $applicant->applicant->getFullName() !!}  </p>
        </div>
      </div>

      <div class="row">
          <div class="col-3 text-right pr-0">You are hereby appointed as</div>
          <div class="col-4 border border-dark border-top-0 border-right-0 border-left-0 text-center pr-0 pl-0 font-weight-bold">{!! $applicant->applicant->job->plantilla_item->position->Name !!}</div>
          <div class="col-1 text-center pr-0 pl-0">(SG/JG/PG)</div>
          <div class="col-4 border border-dark border-top-0 border-right-0 border-left-0 text-center font-weight-bold">{!! $applicant->applicant->job->plantilla_item->salary_grade->Name !!}</div>
      </div>

      <div class="row mb-2">
          <div class="col-3"></div>
          <div class="col-4 text-center">(Position Titlte)</div>
      </div>

      <div class="row">
          <div class="col-1 text-left pl-3 pr-0">under</div>
          <div class="col-6 border border-dark border-top-0 border-right-0 border-left-0 text-center pl-0 pr-0 font-weight-bold">{!! config('params.employee_status.'.$applicant->employee_status) !!}</div>
          <div class="col-1 text-center pl-0 pr-0">status at the</div>
          <div class="col-4 border border-dark border-top-0 border-right-0 border-left-0 text-center font-weight-bold">{!! $applicant->applicant->job->plantilla_item->division->Name !!}</div>
      </div>

      <div class="row mb-2">
          <div class="col-8 text-center">(Permanent,Temporary,etc.)</div>
          <div class="col-4 text-center">(Office/Department/Unit)</div>
      </div>

      <div class="row mb-6">
        <div class="pr-0 pl-3">with compensation rate of</div>
        <div class="col-5 border border-dark border-top-0 border-right-0 border-left-0 text-center font-weight-bold"> <span style="font-size: 12px;">{{ strtoupper($number_in_word) }}</span></div>
        <div class="pl-0 pr-0">(P</div>
        <div class="col-2 border border-dark border-top-0 border-right-0 border-left-0 text-center pl-0 pr-0 font-weight-bold">{{ number_format($applicant->applicant->job->monthly_basic_salary,2) }}</div>
        <div class="col-2 pl-0 pr-0 text-left">) pesos per month.</div>
      </div>


      <div class="row">
          <div class="col-3 text-right pr-0">The nature of appointment is</div>
          <div class="col-4 border border-dark border-top-0 border-right-0 border-left-0 text-center pr-0 pl-0 font-weight-bold">{!! config('params.nature_of_appointment.'.$applicant->nature_of_appointment) !!}</div>
          <div class="col-1 text-center pr-0 pl-0">vice</div>
          <div class="col-4 border border-dark border-top-0 border-right-0 border-left-0 text-center pl-0 pr-0"></div>
      </div>

      <div class="row mb-2">
          <div class="col-10 text-center pr-0 pl-0">(Original, Promotion, etc.)    </div>
      </div>

      <div class="row mb-1">
        <div class="col-1 text-left pr-0">
            who
        </div>
        <div class="col-3 border border-dark border-top-0 border-right-0 border-left-0 text-center pr-0 pl-0"></div>
        <div class="pr-0 pl-0 text-center">with Plantilla Item No.</div>
        <div class="col-4 border border-dark border-top-0 border-right-0 border-left-0 text-center pr-0 pl-0 font-weight-bold">{!! $applicant->applicant->job->plantilla_item->Name !!}</div>
      </div>

      <div class="row mb-2">
          <div class="col-5 text-center pr-0 pl-0">(Transferred, Retired, etc.)    </div>
      </div>

      <div class="row mb-4">
          <div class="col-1 pr-0">Page No. </div>
          <div class="col-2 border border-dark border-top-0 border-right-0 border-left-0"></div>
      </div>

      <div class="row mb-8">
          <div class="col-8 text-right pr-0">This appointment shall take effect on the date of signing by the appointing officer/authority.</div>
      </div>

      <div class="row mb-6">
        <div class="col-sm-7"></div>
        <div class="col-sm-5">Very truly yours,</div>
      </div>

      <div class="row">
        <div class="col-sm-7"></div>
        <div class="col-sm-3 text-center">{!! $applicant->appointing_officer !!}</div>
      </div>

      <div class="row mb-6">
        <div class="col-sm-7"></div>
        <div class="col-sm-3 border-top text-center"> Appointing Officer/Authority</div>
      </div>


      <div class="row">
        <div class="col-sm-7"></div>
        <div class="col-sm-3 text-center">{!! $applicant->date_sign !!}</div>
      </div>

      <div class="row mb-1">
        <div class="col-sm-7"></div>
        <div class="col-sm-3 border-top text-center"> Date of Signing</div>
      </div>

      <div style="height: 30em;"></div>

        <div class="row mb-1">
            <div class="col-sm-7">
                Accredited/Deregulated Pursuant to <br>
                CSC Resolution No. _____, s. _____ <br>
                dated

            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 text-center">
                <img src="{{ url('img/seal.png') }}" >
            </div>
            <div class="col-sm-8"></div>
        </div>

        <div class="row">
            <div class="col-sm-12 text-right"><i style="font-size: 10px;">(Stamp of Date of Release)</i></div>
        </div>
    </div>

    <div style="page-break-before:always; "></div>
    <br>
<!--     <div class="report1" style="border: 10px solid #b1acac;padding: 10px;">
        <div class="row mb-8">
            <div class="col-sm-12 font-weight-bold">CSC ACTION:</div>
        </div>

        <div class="row mb-1">
            <div class="col-1"></div>
            <div class="col-sm-3 border-top"></div>
        </div>
        <div class="row mb-8">
            <div class="col-1"></div>
            <div class="col-sm-3 text-center font-weight-bold">Authorize Official</div>
        </div>

        <div class="row mb-1">
            <div class="col-1"></div>
            <div class="col-sm-3 border-top"></div>
        </div>
        <div class="row mb-5">
            <div class="col-1"></div>
            <div class="col-sm-3 text-center font-weight-bold">Date</div>
        </div>
        <div class="row mb-2">
            <div class="col-sm-12 text-right"><i>(Stamp of Date of Release)</i></div>
        </div>
    </div> -->

    <div class="report1" style="border: 15px solid #b1acac;padding: 14px;">
        <div class="row mb-2">
            <div class="col-sm-12 font-weight-bold text-center" style="font-size: 14pt;">Certification</div>
        </div>

        <div class="offset-2 text-justify mb-1" style="text-justify:inter-word;">
          This is to certify that all requirements and supporting papers pursuant to <b>CSC MC No. 24, s. 2017</b>,
        </div>

        <div class="text-justify offset-1 mb-1">
          <b>as amended,</b> have been complied with, reviewed and found to be in order.
        </div>

        <div class="text-justify offset-2 mb-1">
            The position was published at _______________________________ from __________ to _______,
        </div>

        <div class="text-justify offset-1 mb-1">
            20_____ and posted in _____________________________________ from___________ to__________,
        </div>

        <div class="text-justify offset-1 mb-1">
            20_____ in consonance with RA No. 7041. The assessment by the Human Resource Merit Promotion and
        </div>

        <div class="text-justify offset-1 mb-8">
            Selection Board (HRMPSB) started on ______________, 20_____.
        </div>


        <div class="row">
            <div class="col-sm-7"></div>
            <div class="col-sm-3 text-center">{!! $applicant->hrmo !!}</div>
          </div>

        <div class="row mb-6">
            <div class="col-sm-7"></div>
            <div class="col-sm-3 border-top border-dark text-center font-weight-bold">HRMO</div>
          </div>
    </div>

    <div class="report1" style="border: 15px solid #b1acac;padding: 14px;">
        <div class="row mb-2">
            <div class="col-sm-12 font-weight-bold text-center" style="font-size: 14pt;">Certification</div>
        </div>

        <div class="text-justify offset-2 mb-1">
          This is to certify that the appointee has been screened and found qualified by the majority
        </div>

        <div class="text-justify offset-1 mb-1">
            of the HRMPSB/Placement Committee during the deliberation held on
        </div>

        <div class="text-justify offset-1 mb-7 border-bottom border-dark col-2">
          &nbsp;
        </div>


        <div class="row">
            <div class="col-sm-7"></div>
            <div class="col-sm-3 text-center"> {!! $applicant->chairperson !!}</div>
          </div>

        <div class="row mb-6">
            <div class="col-sm-7"></div>
            <div class="col-sm-3 border-top text-center border-dark"> Chairperson, HRMPSB</div>
          </div>
    </div>
    <div class="report1" style="border: 10px solid #b1acac;padding: 14px;">
        <div class="row mb-2">
            <div class="col-sm-12 font-weight-bold text-center"><h4>CSC Notation</h4></div>
        </div>

        <div class="row form-group">
            <div class="col-sm-12">
                <table class="table1 table-bordered col-12">
                    <thead>
                        <tr>
                            <th colspan="3" class="text-center">ACTION ON APPOINTMENTS</th>
                            <th>Recorded By</th>
                        </tr>
                        <tr>
                            <th colspan="3"><input type="checkbox">  Validated per RAI for the month of   </th>
                            <th></th>
                        </tr>
                        <tr>
                            <th colspan="3"><input type="checkbox"> Invalidated per CSCRO/FO letter dated    </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                         <tr >
                            <td><input type="checkbox"> Appeal</td>
                            <td class="text-center">DATE FILED</td>
                            <td class="text-center">STATUS</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><p class="p-0 m-0" style="text-indent: 15px;"><input type="checkbox"> CSCRO/ CSC-Commission </p></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr >
                            <td><input type="checkbox"> Petition for Review</td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><p class="p-0 m-0" style="text-indent: 15px;"><input type="checkbox"> CSC-Commission  </p></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><p class="p-0 m-0" style="text-indent: 15px;"><input type="checkbox"> Court of Appeals</p></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><p class="p-0 m-0" style="text-indent: 15px;"><input type="checkbox"> Supreme Court</p></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>

    </div>
    <br>
<!--     <div class="report1" style="border: 10px solid #b1acac;padding: 14px;">

        <div class="row mb-6"></div>
        <div class="row mb-4">
            <div class="col-sm-12">
                <p style="text-indent: 50px;" class="text-justify">ANY ERASURE OR ALTERATION ON THE CSC ACTION SHALL NULLIFY OR INVALIDATE THIS APPOINTMENT EXCEPT IF THE ALTERATION WAS AUTHORIZED BY THE COMMISSION. </p>
            </div>
        </div>
    </div> -->
    <br>
    <div class="report1" style="border: 10px solid #b1acac;padding: 14px;">

        <div class="row mb-1">
            <div class="col-sm-6">
                <div class="mb-6"></div>
                <ul style="list-style: none;">
                    <li>Original Copy   -   for the Appointee</li>
                    <li>Original Copy   -   for the Civil Service Commission</li>
                    <li>Original Copy   -   for the Agency</li>
                </ul>
            </div>
            <div class="col-sm-6 text-center">
                <p class="font-weight-bold">Acknowledgement</p>
                <p>Received original/photocopy of appointment on_____________</p>
            </div>
        </div>

        <div class="row mb-6">
            <div class="col-sm-7"></div>
            <div class="col-sm-3 border-top text-center border-dark"> Highest Ranking HRMO</div>
          </div>
    </div>

</div>
@endif
 <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection