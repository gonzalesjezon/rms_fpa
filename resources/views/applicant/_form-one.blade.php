@if(!$applicant->job_id)
    <div class="form-group row">
        <label class="col-12 col-sm-3 col-form-label text-sm-right">Position Applied<span style="color:red;">*</span> </label>
        <div class="col-12 col-sm-8 col-lg-6">
            <select class="form-control form-control-xs" name="job_id" required="true">
                <option>Select position</option>
                @foreach($jobs as $job)
                <option value="{{$job->id}}" {{ ($job->id == $applicant->job_id) ? 'selected' : '' }}>{{$job->plantilla_item->position->Name}}</option>
                @endforeach
            </select>
        </div>
    </div>

@else
    <div class="form-group row">
        <label class="col-12 col-sm-3 col-form-label text-sm-right">Position Applied <span style="color:red;">*</span> </label>
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('job_id', $jobs->title, [
                            'class' => 'form-control form-control-sm',
                            'placeholder' => 'Vacant Position ID',
                            'required' => $applicant->job_id ? false : true,
                            'disabled' => true,
                        ])
                    }}
            {!! $errors->first('job_id', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            {{ Form::hidden('job_id', $applicant->job_id) }}
        </div>
    </div>
@endif

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">First Name <span style="color:red;">*</span> </label>
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('first_name', $applicant->first_name, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'First Name',
                'required' => true,
            ])
        }}
        {!! $errors->first('first_name', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('middle_name', 'Middle Name', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('middle_name', $applicant->middle_name, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Middle Name',
            ])
        }}
        {!! $errors->first('middle_name', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Last Name <span style="color:red;">*</span> </label>
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('last_name', $applicant->last_name, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Last Name',
                'required' => true,
            ])
        }}
        {!! $errors->first('last_name', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('extension_name', 'Extension Name', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('extension_name', $applicant->extension_name, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Extension Name',
            ])
        }}
        {!! $errors->first('extension_name', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('nickname', 'Nick Name', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('nickname', $applicant->nickname, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Nickname',
            ])
        }}
        {!! $errors->first('nickname', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Email Address <span style="color:red;">*</span> </label>
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('email_address', $applicant->email_address, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Email Address',
                'required' => true
            ])
        }}
        {!! $errors->first('email_address', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Mobile Number <span style="color:red;">*</span> </label>
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('mobile_number', $applicant->mobile_number, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Mobile Number',
                'required' => true,
            ])
        }}
        {!! $errors->first('mobile_number', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('telephone_number', 'Telephone Number', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('telephone_number', $applicant->telephone_number, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Telephone Number',
            ])
        }}
        {!! $errors->first('telephone_number', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Where did you find this vacant position? <span style="color:red;">*</span> </label>
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::select('publication', config('params.publication'), $applicant->publication, [
                'class' => 'form-control form-control-xs',
                'placeholder' => 'Where did you find this vacant position?',
                'required' => true,
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('gwa', 'GWA', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right'])}}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('gwa', $applicant->gwa, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'GWA',
            ])
        }}
        {!! $errors->first('gwa', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>
<!--
<div class="form-group row">
    {{ Form::label('image_path', 'Profile Picture', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        <input id="image_path" type="file" name="image_path" data-multiple-caption="{count} files selected" multiple=""
               class="inputfile">
        <label for="image_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
        <span class="badge badge-primary">{{ $applicant->image_path }}</span>
        @if($applicant->image_path)
            <img src="{{ asset('storage/images/' . $applicant->image_path) }}" class="img-thumbnail rounded float-right" width="200" height="200" alt="profile picture">
        @endif
    </div>
</div> -->

<div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
        {{ Form::button('Next Step', ['id' => 'btn-form-one', 'class'=>'btn btn-primary btn-space wizard-next', 'data-wizard' => '#wizard1']) }}
        {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
    </div>
</div>