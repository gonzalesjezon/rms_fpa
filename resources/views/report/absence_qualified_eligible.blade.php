@extends('layouts.print')

@section('css')
@endsection

@section('content')
<div class="reports" style="width: 760px;margin: auto;font-size: 14px;font-family: Arial, Helvetica, sans-serif;">
	<div class="row mb-6">
		<div class="col-sm-12">
			CS Form No. 5 <br>
			Series of 2018
		</div>
	</div>

	<div class="row mb-6">
		<div class="col-sm-12 text-center">
			<p class="m-0 p-0 font-weight-bold" style="font-size: 21px;">Republic of the Philippines</p>
			<div style="font-size: 12pt;" class="font-weight-bold">FERTILIZER AND PESTICIDE AUTHORITY</div>
		    <div>FPA Bldg, B.A.I Compound Visayas Ave,</div>
		    <div>Diliman, Quezon City, Metro Manila</div>
		</div>
	</div>

	<div class="row mb-4">
    	<div class="col-12 text-center">
      		<h3><b>CERTIFICATION</b></h3>
    	</div>
  	</div>

	<div class="row mb-1">
		<div class="col-1"></div>
		<div class="col-11">
			This is to certify that based on the records of this Office, there is no applicant who meets all the qualification
		</div>
	</div>

	<div class="row mb-6">
		<div class="col-12">requirements to the to the <u>(Position Title)</u> position in the <u>(Name of Office/Agency Name)</u>, <u>(Location)</u>. </div>
	</div>

	<div class="row mb-2">
		<div class="col-12">
			<p style="text-indent: 60px;" class="text-justify">This certification is issued pursuant to Section 5 (k), Rule II of <b>CSC Memorandum No. 24, s. 2017</b> (2017 Omnibus Rules on Appointments and Other Human Resource Actions), as <b>ammended</b> </p>
		</div>
	</div>

	<div class="row mb-8">
		<div class="col-12">
			<p style="text-indent: 60px;" class="text-justify">I agree that any misrepresentation made in this certification shall cause the filing of administrative/criminal case/s against me. </p>
		</div>
	</div>

	<div class="row">
		<div class="col-8"></div>
		<div class="col-4 text-center border-bottom border-dark">
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-8"></div>
		<div class="col-4 text-center">
			 Highest Official In Charge of HRM
		</div>
	</div>

	<div class="row mb-6">
		<div class="col-4 text-center">
			Date: _________________
		</div>
	</div>
</div>


 <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection