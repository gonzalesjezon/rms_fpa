@extends('layouts.print')

@section('css')
<style type="text/css">
  @media print{
    @page{
      size: a4 landscape;
    }
    .table>thead>tr>th{
      padding: 3px !important;
      border: 1px solid #000;
    }
  }
  .table>thead>tr>th, .table>tbody>tr>td{
    padding: 3px !important;
    border: 1px solid #000;
  }
</style>
@endsection

@section('content')

<div id="reports" style="margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
  <div class="row mb-2">
    <div class="col-sm-3">CS Form No. 1 <br> <i>Revised 2018</i></div>
    <div class="col-sm-9 text-right"><span class="border border-dark p-1">For Use of Accredited Agencies Only</span></div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-12 text-center"><h4><b>APPOINTMENT TRANSMITTAL  AND ACTION FORM</b></h4></div>
  </div>

  <div class="row mb-6">
    <div class="col-sm-1 text-right">Agency</div>
    <div class="col-sm-5 border-bottom border-dark">FERTILIZER AND PESTICIDE AUTHORITY</div>

    <div class="col-sm-2 text-right"></div>

    <div class="col-sm-2 text-right">CSCFO In-charge:</div>
    <div class="col-sm-2 border-bottom border-dark">&nbsp;</div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-12">INSTRUCTIONS:</div>
  </div>
  <div class="row mb-1">
  	<div class="col-sm-6">
  		<ol>
  			<li>Fill-out the data needed in the form completely and accurately.</li>
  			<li>Do not abbreviate entries in the form.</li>
  			<li>Accomplish the Checklist of Common Requirements and sign the certification.</li>
  			<li>Submit the duly accomplished form in electronic and printed copy (2 copies) to the CSC Field Office-in-Charge together with the original CSC copy of appointments and supporting documents. </li>
  		</ol>
  	 </div>

  	 <div class="col-sm-2"></div>
  	 <div class="col-sm-4 border border-dark text-left" style="padding-right: 0px !important;padding-left: 0px !important;">
  	 	<p class="mb-0 p-0 bg-thead border border-dark border-top-0 border-left-0 border-right-0 border-bottom-1">For CSC RO/FO's Use:</p>
  	 	<p>Date Received:</p>
  	 </div>
  </div>

  <div class="row mb-1">
      <div class="col-sm-12">
        <table id="table1" class="table table-striped table-hover table-fw-widget table-bordered" style="font-size: 10px;">
          <thead>
            <tr class="text-center">
              <th rowspan="2">No</th>
              <th colspan="4">Name of Appointers</th>
              <th rowspan="2" style="vertical-align: middle;">Position <br> Title</th>
              <th rowspan="2" style="vertical-align: middle;">Salary/ <br>JOB/ <br>Pay Grade</th>
              <th rowspan="2" style="vertical-align: middle;">Salary Rate <br>(Annual)</th>
              <th rowspan="2" style="vertical-align: middle;">Employement <br> Status</th>
              <th rowspan="2" style="vertical-align: middle;">PERIOD OF EMPLOYMENT <br> (for Temporary, <br> Casual/ <br> Contractual Appointments) </th>
              <th rowspan="2" style="vertical-align: middle;">NATURE OF <br> APPOINTMENT</th>
              <th rowspan="2" style="vertical-align: middle;">DATE OF <br> ISSUANCE</th>
              <th colspan="2">Publication</th>
              <th colspan="3">CSC Action</th>
              <th rowspan="2" style="vertical-align: middle;">Agency Receiving <br> Officer</th>
            </tr>
            <tr class="text-center">
              <th style="vertical-align: middle;">Last Name</th>
              <th style="vertical-align: middle;">First Name</th>
              <th style="vertical-align: middle;">Name Extension <br> (Jr./III)</th>
              <th style="vertical-align: middle;">Middle Name</th>
              <th>DATE indicate period of publication <br> (mm/dd/yyyy to mm/dd/yyyy)</th>
              <th style="vertical-align: middle;">MODE <br> (CSC Bulletin of Vacant Positions) </th>
              <th>V-Validated <br> INV- Invalidated</th>
              <th style="vertical-align: middle;">Date of Action</th>
              <th style="vertical-align: middle;">Date of Release</th>
            </tr>
          </thead>
          <tbody>
        </table>
      </div>
  </div>
  <div class="row mb-4">
  	  <div class="col-4"></div>
      <div class="col-sm-5 text-left">
      	<p class="font-weight-bold mb-2">CERTIFCATION</p>
      	<p class="mb-4">This is to certify that the information contained in this form are true, correct and complete.</p>
      	<p class="border border-bottom border-dark pb-0 mb-0"></p>
      	<p class="text-center p-0 m-0 mb-2"><span>HMO</span></p>
      	<p>Date ____________________________</p>
      </div>
  </div>

  <div class="col-sm-12 border-dark border" style="padding-right: 0px !important;padding-left: 0px !important;">
  	<p class="border border-bottom-1 border-dark border-top-0 border-left-0 border-right-0 p-0 m-0">REMARKS/COMMENTS/RECOMMENDATIONS: (e.g. Reasons for Disapproval of Appointment)</p>
  	<p class="border border-bottom-1 border-dark border-top-0 border-left-0 border-right-0 p-0 m-0">&nbsp;</p>
  	<p class="p-0 m-0">&nbsp;</p>
  </div>

</div>

 <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection