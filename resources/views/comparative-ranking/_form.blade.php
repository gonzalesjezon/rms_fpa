@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
        rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/styles.css') }}" />
@endsection

<table id="table1" class="table table-striped table-hover table-fw-widget table-bordered" style="font-size: 11px !important;">
  <thead>
  <tr class="text-center">
    <th>NAME</th>
    <th >PERFORMANCE 23%</th>
    <th >EDUCATION AND TRAINING 20%</th>
    <th >EXPERIENCE AND OUTSTANDING ACCOMPLISHMENTS 22%</th>
    <th >PYSCHOSOCIAL 20%</th>
    <th >POTENTIAL 20%</th>
    <th >EXAMINATION 5%</th>
    <th >Recommend</th>
  </tr>
  </thead>
  <tbody>
  {!! Form::open(['action' => $action, 'method' => $method, 'id' => 'matrix-form']) !!}
    <tr >
      <td class="text-center">{{$evaluation->applicant->getFullname()}}</td>
      <td class="text-center">{{$evaluation->performance_score}}</td>
      <td class="text-center">{{$evaluation->education_training_score}}</td>
      <td class="text-center">{{$evaluation->experience_accomplishments_score}}</td>
      <td class="text-center">{{$evaluation->psychosocial_score}}</td>
      <td class="text-center">{{$evaluation->potential_score}}</td>
      <td class="text-center">{{$evaluation->examination_score}}</td>
      <td>
          <div class="col-12 col-sm-8 col-lg-6 pt-1">
              <div class="switch-button switch-button-success switch-button-yesno">
                  @if($evaluation->recommended == 1)
                  <input type="checkbox" name="recommended" id="swt0" checked="true"><span>
                  @else
                  <input type="checkbox" name="recommended" id="swt0"><span>
                  @endif
                  <label for="swt0"></label></span>
              </div>
          </div>
          <input type="hidden" name="applicant_id" value="{{$evaluation->applicant_id}}">
          <input type="hidden" name="id" value="{{$evaluation->id}}">
          <input type="hidden" name="job_id" value="{{$evaluation->applicant->job_id}}">
        </td>
    </tr>
  </tbody>
</table>


<div class="form-group row text-right">
  <div class="col col-sm-12 ">
    {{ Form::submit('Save', ['id' => 'attestation-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>
{!! Form::close() !!}

@section('scripts')
  <!-- JS Libraries -->
  <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
          type="text/javascript"></script>
  <!-- wysiwyg -->
  <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.formElements();

    });
  </script>
@endsection
