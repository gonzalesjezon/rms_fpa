@section('css')
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form']) !!}
<div class="row">
    <div class="col text-light bg-secondary">I. PERFORMANCE (23%)</div>
</div>

<div class="form-group row text-center">
    <div class="col-6"></div>
    <div class="col">
        {{ Form::text('performance_average', $evaluation->performance_average, [
                'class' => 'form-control form-control-xs performance',
                'id' => 'performance_average'
            ])
        }}
        <label for="" class="mt-2">Total Points</label>
    </div>

    <div class="col"><h4 class="mt-1">X</h4></div>
    <div class="col">
        {{ Form::text('performance_percent', 23, [
                'class' => 'form-control form-control-xs performance',
                'id' => 'performance_percent',
                'readonly' => 'true'
            ])
        }}
        <label for="" class="mt-2">POINTS WEIGHT</label>
    </div>

    <label for=""><h4 class="mt-1">=</h4></label>
    <div class="col">
        {{ Form::text('performance_score', $evaluation->performance_score, [
                'class' => 'form-control form-control-xs performance',
                'id' => 'performance_score',
                'readonly' => 'true'
            ])
        }}
    </div>
</div>

<!-- Education & Training -->
<div class="row">
    <div class="col text-light bg-secondary">II. EDUCATION & TRAINING (20%)</div>
</div>

<div class="row mt-3">
    {{ Form::label('education_points', 'Education Requirement', ['class'=>'col-2 mt-2']) }}
    <label for=""><h4 class="mt-3 font-weight-bold">=</h4></label>
    <div class="col-2">
        {{ Form::text('education_points', $evaluation->education_points, [
                'class' => 'form-control form-control-xs education_training',
                'id' => 'education_points'
            ])
        }}
    </div>
</div>

<div class=" row">
    {{ Form::label('graduate_points', 'Graduate Studies', ['class'=>'col-2 mt-2']) }}
    <label for=""><h4 class="mt-3 font-weight-bold">=</h4></label>
    <div class="col-2">
        {{ Form::text('graduate_points', $evaluation->graduate_points, [
                'class' => 'form-control form-control-xs education_training',
                'id' => 'graduate_points'
            ])
        }}
    </div>

    <label for="" class=" mt-3">=</label>
    <div class="col-2">
        {{ Form::text('education_graduate_total_points', $evaluation->education_graduate_total_points, [
                'class' => 'form-control form-control-sm education_training',
                'id' => 'education_graduate_total_points',
                'readonly' => 'true'
            ])
        }}
    </div>

    <div class="mt-3">X</div>
    <div class="col-3">
        {{ Form::text('education_graduate_percent', 20, [
                'class' => 'form-control form-control-sm education_training',
                'id' => 'education_graduate_percent',
                'readonly' => 'true'
            ])
        }}
    </div>

    <label for="" class="mt-3">=</label>
    <div class="col-2">
        {{ Form::text('education_graduate_score', $evaluation->education_graduate_score, [
                'class' => 'form-control form-control-sm',
                'id' => 'education_graduate_score',
                'readonly' => 'true'
            ])
        }}
    </div>
</div>

<div class="row text-right">
    {{ Form::label('', 'TOTAL', ['class'=>'col-2 mt-2']) }}
    <label for=""><h4 class="mt-3 font-weight-bold">=</h4></label>
    <div class="col-2">
        {{ Form::text('educ_total', '', [
                'class' => 'form-control form-control-xs',
                'id' => 'educ_total',
                'readonly' => 'true'
            ])
        }}
    </div>
    {{ Form::label('', 'TOTAL POINTS', ['class'=>'col-2 text-center']) }}
    {{ Form::label('', 'POINTS WEIGHT', ['class'=>'col-3 text-center']) }}
</div>

<!-- Experience & Outstanding Accomplishments -->
<div class="form-group row">
    <div class="col text-light bg-secondary">III. EXPERIENCE & OUTSTANDING ACCOMPLISHMENTS (22%)</div>
</div>

<div class="row mt-3">
    {{ Form::label('experience_points', 'Work Experience Requirement', ['class'=>'col-2 mt-2']) }}
    <label for=""><h4 class="mt-3 font-weight-bold">=</h4></label>
    <div class="col-2">
        {{ Form::text('experience_points', $evaluation->experience_points, [
                'class' => 'form-control form-control-xs experience',
                'id' => 'experience_points'
            ])
        }}
    </div>
</div>

<div class=" row">
    {{ Form::label('number_of_year_points', 'No. of Years', ['class'=>'col-2 mt-2']) }}
    <label for=""><h4 class="mt-3 font-weight-bold">=</h4></label>
    <div class="col-2">
        {{ Form::text('number_of_year_points', $evaluation->number_of_year_points, [
                'class' => 'form-control form-control-xs experience',
                'id' => 'number_of_year_points'
            ])
        }}
    </div>

    <label for="" class=" mt-3">=</label>
    <div class="col-2">
        {{ Form::text('experience_total_points', $evaluation->experience_total_points, [
                'class' => 'form-control form-control-sm experience',
                'id' => 'experience_total_points',
                'readonly' => 'true'
            ])
        }}
    </div>

    <div class="mt-3">X</div>
    <div class="col-3">
        {{ Form::text('experience_percent', 22, [
                'class' => 'form-control form-control-sm experience',
                'id' => 'experience_percent',
                'readonly' => 'true'
            ])
        }}
    </div>

    <label for="" class="mt-3">=</label>
    <div class="col-2">
        {{ Form::text('experience_score', $evaluation->experience_score, [
                'class' => 'form-control form-control-sm',
                'id' => 'experience_score',
                'readonly' => 'true'
            ])
        }}
    </div>
</div>

<div class="row text-right">
    {{ Form::label('', 'TOTAL', ['class'=>'col-2 mt-2']) }}
    <label for=""><h4 class="mt-3 font-weight-bold">=</h4></label>
    <div class="col-2">
        {{ Form::text('experience_total', '', [
                'class' => 'form-control form-control-xs education_training',
                'id' => 'experience_total',
                'readonly' => 'true'
            ])
        }}
    </div>
    {{ Form::label('', 'TOTAL POINTS', ['class'=>'col-2 text-center']) }}
    {{ Form::label('', 'POINTS WEIGHT', ['class'=>'col-3 text-center']) }}
</div>

<!-- Potential -->
<div class="row">
    <div class="col text-light bg-secondary">IV. POTENTIAL (10%)</div>
</div>

<div class="row mt-3">
    {{ Form::label('', 'R1', ['class'=>'col-2 mt-2']) }}
    <div class="col-2">
        {{ Form::text('potential_r1', $evaluation->potential_r1, [
                'class' => 'form-control form-control-xs potential',
                'id' => 'potential_r1'
            ])
        }}
    </div>
    <label class="text-left mt-2">pts</label>
</div>

<div class="row">
    {{ Form::label('', 'R2', ['class'=>'col-2 mt-2']) }}
    <div class="col-2">
        {{ Form::text('potential_r2', $evaluation->potential_r2, [
                'class' => 'form-control form-control-xs potential',
                'id' => 'potential_r2'
            ])
        }}
    </div>
    <label class="text-left mt-2">pts</label>
</div>

<div class="row">
    {{ Form::label('', 'R3', ['class'=>'col-2 mt-2']) }}
    <div class="col-2">
        {{ Form::text('potential_r3', $evaluation->potential_r3, [
                'class' => 'form-control form-control-xs potential',
                'id' => 'potential_r3'
            ])
        }}
    </div>
    <label class="text-left mt-2">pts</label>

    <div class="col-2">
        {{ Form::text('potential_total_points', '', [
                'class' => 'form-control form-control-sm potential',
                'id' => 'potential_total_points',
                'readonly' => 'true'
            ])
        }}
    </div>

    <div class="mt-3">X</div>
    <div class="col-3">
        {{ Form::text('potential_percent', 10, [
                'class' => 'form-control form-control-sm potential',
                'id' => 'potential_percent',
                'readonly' => 'true'
            ])
        }}
    </div>

    <label for="" class="mt-3">=</label>
    <div class="col-2">
        {{ Form::text('potential_score', $evaluation->potential_score, [
                'class' => 'form-control form-control-sm',
                'id' => 'potential_score',
                'readonly' => 'true'
            ])
        }}
    </div>
</div>

<div class="row">
    {{ Form::label('', 'R4', ['class'=>'col-2 mt-2']) }}
    <div class="col-2">
        {{ Form::text('potential_r4', $evaluation->potential_r4, [
                'class' => 'form-control form-control-xs potential',
                'id' => 'potential_r4'
            ])
        }}
    </div>
    <label class="text-left mt-2">pts</label>

    {{ Form::label('', 'TOTAL POINTS', ['class'=>'col-2 text-center']) }}
    {{ Form::label('', 'POINTS WEIGHT', ['class'=>'col-3 text-center']) }}
</div>

<div class="row text-right">
    {{ Form::label('', 'TOTAL', ['class'=>'col-2 mt-2']) }}
    <div class="col-2">
        {{ Form::text('potential_total', '', [
                'class' => 'form-control form-control-xs',
                'id' => 'potential_total',
                'readonly' => 'true'
            ])
        }}
    </div>
</div>

<!-- Pschosocial -->
<div class=" row">
    <div class="col text-light bg-secondary">V. PSYCHOSOCIAL ATTRIBUTES & PERSONALITY TRAITS (20%)</div>
</div>

<div class="row mt-3">
    {{ Form::label('', 'R1', ['class'=>'col-2 mt-2']) }}
    <div class="col-2">
        {{ Form::text('psychosocial_r1', $evaluation->psychosocial_r1, [
                'class' => 'form-control form-control-xs psychosocial',
                'id' => 'psychosocial_r1'
            ])
        }}
    </div>
    <label class="text-left mt-2">pts</label>
</div>

<div class="row">
    {{ Form::label('', 'R2', ['class'=>'col-2 mt-2']) }}
    <div class="col-2">
        {{ Form::text('psychosocial_r2', $evaluation->psychosocial_r2, [
                'class' => 'form-control form-control-xs psychosocial',
                'id' => 'psychosocial_r2'
            ])
        }}
    </div>
    <label class="text-left mt-2">pts</label>
</div>

<div class="row">
    {{ Form::label('', 'R3', ['class'=>'col-2 mt-2']) }}
    <div class="col-2">
        {{ Form::text('psychosocial_r3', $evaluation->psychosocial_r3, [
                'class' => 'form-control form-control-xs psychosocial',
                'id' => 'psychosocial_r3'
            ])
        }}
    </div>
    <label class="text-left mt-2">pts</label>

    <div class="col-2">
        {{ Form::text('psychosocial_total_points', '', [
                'class' => 'form-control form-control-sm psychosocial',
                'id' => 'psychosocial_total_points',
                'readonly' => 'true'
            ])
        }}
    </div>

    <div class="mt-3">X</div>
    <div class="col-3">
        {{ Form::text('psychosocial_percent', 10, [
                'class' => 'form-control form-control-sm psychosocial',
                'id' => 'psychosocial_percent',
                'readonly' => 'true'
            ])
        }}
    </div>

    <label for="" class="mt-3">=</label>
    <div class="col-2">
        {{ Form::text('psychosocial_score', $evaluation->psychosocial_score, [
                'class' => 'form-control form-control-sm',
                'id' => 'psychosocial_score',
                'readonly' => 'true'
            ])
        }}
    </div>
</div>

<div class="row">
    {{ Form::label('', 'R4', ['class'=>'col-2 mt-2']) }}
    <div class="col-2">
        {{ Form::text('psychosocial_r4', $evaluation->psychosocial_r4, [
                'class' => 'form-control form-control-xs psychosocial',
                'id' => 'psychosocial_r4'
            ])
        }}
    </div>
    <label class="text-left mt-2">pts</label>

    {{ Form::label('', 'TOTAL POINTS', ['class'=>'col-2 text-center']) }}
    {{ Form::label('', 'POINTS WEIGHT', ['class'=>'col-3 text-center']) }}
</div>

<div class="row text-right">
    {{ Form::label('', 'TOTAL', ['class'=>'col-2 mt-2']) }}
    <div class="col-2">
        {{ Form::text('psychosocial_total', '', [
                'class' => 'form-control form-control-xs',
                'id' => 'psychosocial_total',
                'readonly' => 'true'
            ])
        }}
    </div>
</div>

<!-- Examination -->
<div class="row">
    <div class="col text-light bg-secondary">VI. EXAMINATION (5%)</div>
</div>

<div class="form-group row text-center">
     <div class="col-6"></div>
    <div class="col">
        {{ Form::text('examination_total_points', $evaluation->examination_total_points, [
                'class' => 'form-control form-control-xs examination',
                'id' => 'examination_total_points'
            ])
        }}
        <label for="" class="mt-2">Total Points</label>
    </div>

    <div class="col"><h4 class="mt-1">X</h4></div>
    <div class="col">
        {{ Form::text('examination_percent', 5, [
                'class' => 'form-control form-control-xs',
                'id' => 'examination_percent',
                'readonly' => 'true'
            ])
        }}
        <label for="" class="mt-2">POINTS WEIGHT</label>
    </div>

    <label for=""><h4 class="mt-1">=</h4></label>
    <div class="col">
        {{ Form::text('examination_score', $evaluation->examination_score, [
                'class' => 'form-control form-control-xs',
                'id' => 'examination_score',
                'readonly' => 'true'
            ])
        }}
    </div>
</div>

<div class="row mb-4">
    <div class="col text-light bg-secondary">
        TOTAL
    </div>
</div>

<div class="row text-center">
     <div class="col-8"></div>
    <div class="col">
        {{ Form::text('total_percent', $evaluation->total_percent, [
                'class' => 'form-control form-control-xs',
                'id' => 'total_percent',
                'readonly' => 'true'
            ])
        }}
        <label for="" class="mt-2">POINTS WEIGHT</label>
    </div>

    <label for=""><h4 class="mt-1">=</h4></label>
    <div class="col">
        {{ Form::text('total_score', $evaluation->total_score, [
                'class' => 'form-control form-control-xs ',
                'id' => 'total_score',
                'readonly' => 'true'
            ])
        }}
    <label for="" class="mt-2">SCORE</label>
    </div>
</div>

<hr>
<!-- <div class="form-group row border border-grey border-bottom-0 border-left-0 border-right-0">
    {{ Form::label('evaluated_by', 'Evaluated By', ['class'=>'col-2 mt-2']) }}
    <div class="col-4 mr-6">
        {{ Form::text('evaluated_by', $evaluation->evaluated_by, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Evaluated By',
                'id' => 'evaluated_by'
            ])
        }}
    </div>

</div>

<div class="form-group row">
    {{ Form::label('reviewed_by', 'Reviewed By', ['class'=>'col-2 mt-2']) }}
    <div class="col-4">
        {{ Form::text('reviewed_by', $evaluation->reviewed_by, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Reviewed By',
                'id' => 'reviewed_by'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('noted_by', 'Noted By', ['class'=>'col-2 mt-2']) }}
    <div class="col-4">
        {{ Form::text('noted_by', $evaluation->noted_by, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Noted By',
                'id' => 'noted_by'
            ])
        }}
    </div>
</div> -->

{{ Form::hidden('reference', app('request')->input('reference')) }}

<div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
        {{ Form::button('Save', ['id' => 'btn-form-one', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
        {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
{!! Form::close() !!}


<!-- Nifty Modal-->
<div id="md-flipH" class="modal-container modal-effect-8">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span
                        class="mdi mdi-close"></span></button>
        </div>
        <div id="printThis" class="modal-body">
            <img src="{{ URL::asset('img/merit-selection-plan.png') }}" class="img-fluid" />
        </div>
        <div class="modal-footer">
            <div class="mt-8">
                <button type="button" class="btn btn-info btn-space modal-close" id="print-button">
                    <span class="mdi mdi-print"></span> Print
                </button>
                <button type="button" data-dismiss="modal" class="btn btn-danger btn-space modal-close">
                    <span class="mdi mdi-close"></span> Close
                </button>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    @include('evaluation._form-rating-scripts')
@endsection
