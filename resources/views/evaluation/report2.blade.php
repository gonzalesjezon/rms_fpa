@extends('layouts.print')

@section('css')
<style type="text/css">
  @media print{
    .bg-secondary{
      background-color: #878787 !important;
  }

}
</style>
@endsection

@section('content')
<div style="width: 960px;margin: auto;">
	<div class="row mb-4">
		<div class="col-12 text-center">
			<div style="font-size: 14px;" class="font-weight-bold">FERTILIZER AND PESTICIDE AUTHORITY</div>
            <div>FPA Bldg, B.A.I Compound Visayas Ave,</div>
            <div>Diliman, Quezon City, Metro Manila</div>
        </div>
	</div>

	<div class="row mb-4">
		<div class="col-12 text-center">
			<h4 class="font-weight-bold">COMPARATIVE ASSESSMENT</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<table class="table1 table-bordered">
				<thead>
					<tr>
						<th colspan="2">Position: {!! $applicant->job->plantilla_item->position->Name !!}</th>
						<th colspan="2">Name: {!! $applicant->getFullName() !!} </th>
						<th rowspan="2"></th>
						<th rowspan="2" class="text-center">Total Score <br> {{ $applicant->evaluation->total_score }}</th>
					</tr>
					<tr>
						<th colspan="2">Regulation Area</th>
						<th colspan="2" style="vertical-align: top;">Eligibility:
							{!! $applicant->job->eligibility !!}
						</th>
					</tr>
					<tr class="text-center">
						<th>Performance 23% </th>
						<th>Experience 22%</th>
						<th>Education and Training 20%</th>
						<th>Psychosocial 20%</th>
						<th>Potential 10%</th>
						<th>Exam 5%</th>
					</tr>
				</thead>
				<tbody>
					<tr class="text-center">
						<td style="padding: 150px;" >&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr class="text-center">
						<td >{{ $applicant->evaluation->performance_score }}</td>
						<td>{{ $applicant->evaluation->experience_score }}</td>
						<td>{{ $applicant->evaluation->education_graduate_score }}</td>
						<td>{{ $applicant->evaluation->psychosocial_score }}</td>
						<td>{{ $applicant->evaluation->potential_score }}</td>
						<td>{{ $applicant->evaluation->examination_score }}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

</div>


  <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection