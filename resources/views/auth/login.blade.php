@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col bg-primary text-center text-light h-25"><h2 class="mb-4">Recruitment Management
                    System</h2></div>
        </div>
    </div>
    <div class="be-wrapper be-login  bg-light">
        <div class="be-content">
            <div class="main-content container-fluid bg-light">
                <div class="row">
                    <div class="col-xl-6 offset-xl-1 col-lg-7 col-md-7 col-sm-12 col-xs-12 mt-8 text-center">
                        <img src="{{ URL::asset('beagle-assets/img/pcc-logo-large.png') }}" alt="logo"
                             class="ml-0 img-fluid">
                    </div>
                    <div class="col-xl-3 col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <div class="splash-container">
                            <div class="card card-border-color card-border-color-primary">
                                <div class="card-header">
                                    <span class="splash-description">Please enter your user information.</span>
                                </div>
                                <div class="card-body">

                                    <form method="POST" action="{{ route('login') }}">
                                        {{ csrf_field() }}

                                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                            <input id="username" type="text" placeholder="Username" autocomplete="off"
                                                   class="form-control" name="username" value="{{ old('username') }}" required
                                                   autofocus>
                                            @if ($errors->has('username'))
                                                <ul class="parsley-errors-list filled" id="parsley-id-5">
                                                    <li class="parsley-required">{{ $errors->first('username') }}</li>
                                                </ul>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <input id="password" type="password" placeholder="Password"
                                                   class="form-control" name="password" required>
                                            @if ($errors->has('password'))
                                                <ul class="parsley-errors-list filled" id="parsley-id-5">
                                                    <li class="parsley-required">{{ $errors->first('password') }}</li>
                                                </ul>
                                            @endif
                                        </div>

                                        <div class="form-group row login-tools">
                                            <div class="col-6 login-remember">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox"
                                                           class="custom-control-input" {{ old('remember') ? 'checked' : '' }}><span
                                                            class="custom-control-label">Remember Me</span>
                                                </label>
                                            </div>
                                            <div class="col-6 login-forgot-password">
                                                <a class="btn btn-link" href="{{ route('password.request') }}">Forgot
                                                    Your Password?</a>
                                            </div>
                                        </div>

                                        <div class="form-group login-submit">
                                            <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl">
                                                Sign me in
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                            <div class="splash-footer"><span>Don't have an account? <a href="{{ route('register') }}">Sign Up</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
