@section('css')
      <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/select2/css/select2.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}">
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form', 'files' => true]) !!}

<div class="form-group row">
    <div class="col-2">
        {{ Form::label('applicant', 'Applicant Name', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    </div>
    <div class="col-5">
        {{ Form::label('applicant_name', $applicant->getFullName() ,['class'=>'col-12 col-sm-2 col-form-label text-sm-left font-weight-bold']) }}
    </div>
</div>
<div class="form-group row">
    <div class="col-12">
        <table class="table table-bordered">
            <tbody>
                <tr class="text-center">
                    <td></td>
                    <td class="font-weight-bold">CHECKLIST OF COMMON REQUIREMENTS</td>
                    <td class="font-weight-bold">HRM</td>
                    <td class="font-weight-bold">Attachment</td>
                </tr>
                <tr>
                    <td colspan="4" style="font-size: 12px;">Instructions: Put a check if the requirements are complete. If incomplete, use the space provided to indicate the name of appointee and the lacking requirement/s.</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td><b>Personal Data Sheet</b> (CS Form No. 212, Revised 2017)</td>
                    <td >
                      <div class="col-12 col-sm-8 col-lg-6 offset-3">
                          <div class="switch-button switch-button-success switch-button-yesno">
                                @if(isset($requirement->pds_path))
                                <input type="checkbox" id="swt0" checked="true"><span>
                                @else
                                <input type="checkbox" id="swt0"><span>
                                @endif
                              <label for="swt0"></label></span>
                          </div>
                        </div>
                    </td>
                    <td>
                        <div class="col-12 col-sm-8 col-lg-6 offset-1">
                            <input id="pds_path" type="file" name="pds_path" data-multiple-caption="{count} files selected" multiple=""
                                   class="inputfile">
                            <label for="pds_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                            <span class="badge badge-primary"></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td><b>Statement of Assets, Liabilities and Net Worth (SALN)</b></td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                              <div class="switch-button switch-button-success switch-button-yesno">
                                    @if(@$requirement->saln_path)
                                    <input type="checkbox" id="swt1" checked="true"><span>
                                    @else
                                    <input type="checkbox" id="swt1"><span>
                                    @endif
                                  <label for="swt1"></label></span>
                              </div>
                          </div>
                    </td>
                    <td>
                      <div class="col-12 col-sm-8 col-lg-6 offset-1">
                          <input id="saln_path" type="file" name="saln_path" data-multiple-caption="{count} files selected" multiple=""
                                 class="inputfile">
                          <label for="saln_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                          <span class="badge badge-primary"></span>
                      </div>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td><b>Authenticated Certificate of Civil Service Eligibility or PRC Board Certificate</b></td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->eligibility_path)
                                  <input type="checkbox" id="swt2" checked="true"><span>
                                  @else
                                  <input type="checkbox" id="swt2"><span>
                                  @endif
                                <label for="swt2"></label></span>
                            </div>
                        </div>
                    </td>
                    <td>
                      <div class="col-12 col-sm-8 col-lg-6 offset-1">
                          <input id="eligibility_path" type="file" name="eligibility_path" data-multiple-caption="{count} files selected" multiple=""
                                 class="inputfile">
                          <label for="eligibility_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                          <span class="badge badge-primary"></span>
                      </div>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td><b>Certified True Copies of latest Certificates of Trainging/Seminars attended</b></td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->training_path)
                                  <input type="checkbox" id="swt3" checked="true"><span>
                                  @else
                                  <input type="checkbox" id="swt3"><span>
                                  @endif
                                <label for="swt3"></label></span>
                            </div>
                        </div>
                    </td>
                    <td>
                      <div class="col-12 col-sm-8 col-lg-6 offset-1">
                          <input id="training_path" type="file" name="training_path" data-multiple-caption="{count} files selected" multiple=""
                                 class="inputfile">
                          <label for="training_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                          <span class="badge badge-primary"></span>
                      </div>
                    </td>
                </tr>
                <tr>
                    <td>5</td>
                    <td><b>Copy of Certificate of Live Birth (PSA)</b></td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->psa_path)
                                  <input type="checkbox" id="swt4" checked="true"><span>
                                  @else
                                  <input type="checkbox" id="swt4"><span>
                                  @endif
                                <label for="swt4"></label></span>
                            </div>
                        </div>
                    </td>
                    <td>
                      <div class="col-12 col-sm-8 col-lg-6 offset-1">
                          <input id="psa_path" type="file" name="psa_path" data-multiple-caption="{count} files selected" multiple=""
                                 class="inputfile">
                          <label for="pds_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                          <span class="badge badge-primary"></span>
                      </div>
                    </td>
                </tr>
                <tr>
                    <td>6</td>
                    <td><b>Certified True Copy of Transcript of Records:</b></td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->tor_path)
                                  <input type="checkbox" id="swt5" checked="true"><span>
                                  @else
                                  <input type="checkbox" id="swt5"><span>
                                  @endif
                                <label for="swt5"></label></span>
                            </div>
                        </div>
                    </td>
                    <td>
                      <div class="col-12 col-sm-8 col-lg-6 offset-1">
                          <input id="tor_path" type="file" name="tor_path" data-multiple-caption="{count} files selected" multiple=""
                                 class="inputfile">
                          <label for="tor_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                          <span class="badge badge-primary"></span>
                      </div>
                    </td>
                </tr>
                <tr>
                    <td>7</td>
                    <td><b>Certified True Copy of your Diploma:</b></td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->diploma_path)
                                  <input type="checkbox" id="swt6" checked="true"><span>
                                  @else
                                  <input type="checkbox" id="swt6"><span>
                                  @endif
                                <label for="swt6"></label></span>
                            </div>
                        </div>
                    </td>
                    <td>
                      <div class="col-12 col-sm-8 col-lg-6 offset-1">
                          <input id="diploma_path" type="file" name="diploma_path" data-multiple-caption="{count} files selected" multiple=""
                                 class="inputfile">
                          <label for="diploma_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                          <span class="badge badge-primary"></span>
                      </div>
                    </td>
                </tr>
                <tr>
                    <td>8</td>
                    <td><b>Tax Identification Number</b></td>
                    <td >
                        {{ Form::text('tin_number', '', ['class' => 'form-control form-control-sm']) }}
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>9</td>
                    <td><b>Pagtutulungan sa Kinabukasan: Ikaw, Bangko, Indistriya at Gobyerno (Pagibig Number)</b></td>
                    <td >
                        {{ Form::text('pagibig_number', '', ['class' => 'form-control form-control-sm']) }}
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>10</td>
                    <td><b>Copy of Philhealth MDR</b></td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->philhealth_path)
                                  <input type="checkbox" id="swt7" checked="true"><span>
                                  @else
                                  <input type="checkbox" id="swt7"><span>
                                  @endif
                                <label for="swt7"></label></span>
                            </div>
                        </div>
                    </td>
                    <td>
                      <div class="col-12 col-sm-8 col-lg-6 offset-1">
                          <input id="philhealth_path" type="file" name="philhealth_path" data-multiple-caption="{count} files selected" multiple=""
                                 class="inputfile">
                          <label for="philhealth_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                          <span class="badge badge-primary"></span>
                      </div>
                    </td>
                </tr>
                <tr>
                    <td>11</td>
                    <td><b>Latest National Bureau of Investigation Clearance</b></td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->nbi_path)
                                  <input type="checkbox" id="swt8" checked="true"><span>
                                  @else
                                  <input type="checkbox" id="swt8"><span>
                                  @endif
                                <label for="swt8"></label></span>
                            </div>
                        </div>
                    </td>
                    <td>
                      <div class="col-12 col-sm-8 col-lg-6 offset-1">
                          <input id="nbi_path" type="file" name="nbi_path" data-multiple-caption="{count} files selected" multiple=""
                                 class="inputfile">
                          <label for="nbi_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                          <span class="badge badge-primary"></span>
                      </div>
                    </td>
                </tr>
                <tr>
                    <td>12</td>
                    <td><b>Original Copy of Medical Certificate (Urinalysis, Chest X-ray,CBC, Drug test) indicating "FIT TO WORK"</b></td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->medical_path)
                                  <input type="checkbox" id="swt9" checked="true"><span>
                                  @else
                                  <input type="checkbox" id="swt9"><span>
                                  @endif
                                <label for="swt9"></label></span>
                            </div>
                        </div>
                    </td>
                    <td>
                      <div class="col-12 col-sm-8 col-lg-6 offset-1">
                          <input id="medical_path" type="file" name="medical_path" data-multiple-caption="{count} files selected" multiple=""
                                 class="inputfile">
                          <label for="medical_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                          <span class="badge badge-primary"></span>
                      </div>
                    </td>
                </tr>
                <tr>
                    <td>13</td>
                    <td><b>BIR Form 2316 for those with previous employer</b></td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->bir_path)
                                  <input type="checkbox" id="swt10" checked="true"><span>
                                  @else
                                  <input type="checkbox" id="swt10"><span>
                                  @endif
                                <label for="swt10"></label></span>
                            </div>
                        </div>
                    </td>
                    <td>
                      <div class="col-12 col-sm-8 col-lg-6 offset-1">
                          <input id="bir_path" type="file" name="bir_path" data-multiple-caption="{count} files selected" multiple=""
                                 class="inputfile">
                          <label for="bir_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                          <span class="badge badge-primary"></span>
                      </div>
                    </td>
                </tr>
                <tr>
                    <td>14</td>
                    <td><b>Clearance from previous employer</b></td>
                    <td >
                        <div class="col-12 col-sm-8 col-lg-6 offset-3">
                            <div class="switch-button switch-button-success switch-button-yesno">
                                  @if(@$requirement->coe_path)
                                  <input type="checkbox" id="swt11" checked="true"><span>
                                  @else
                                  <input type="checkbox" id="swt11"><span>
                                  @endif
                                <label for="swt11"></label></span>
                            </div>
                        </div>
                    </td>
                    <td>
                      <div class="col-12 col-sm-8 col-lg-6 offset-1">
                          <input id="coe_path" type="file" name="coe_path" data-multiple-caption="{count} files selected" multiple=""
                                 class="inputfile">
                          <label for="coe_path" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                          <span class="badge badge-primary"></span>
                      </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="form-group row text-right">
  <div class="col col-sm-12 ">
    <input type="hidden" name="applicant_id" value="{{ $applicant->id}}">
    <input type="hidden" name="requirement_id" value="{{ @$requirement->id }}">
    {{ Form::submit('Save', ['id' => 'attestation-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>
{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
      });
    </script>
@endsection


