@section('css')
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/select2/css/select2.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}">
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'attestation-form']) !!}
<div class="form-group row">
   {{ Form::label('', 'Applicant Name', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-5">
        {{ Form::label('', $attestation->applicant->getFullName(), ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    </div>
</div>

<div class="form-group row">
  {{ Form::label('employment_status', 'Employment Status', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-4">
    {{ Form::text('employee_status', config('params.employee_status.'.@$appointmentform->employee_status), [
            'class' => 'form-control form-control-sm',
            'placeholder' => 'Employment Status',
            'readOnly' => 'true'
        ])
    }}
  </div>
  <div class="offset-sm-3 col-sm-5 offset-lg-2 col-lg-3 col-form-label text-sm-left"><strong>CSC Action</strong></div>
</div>

<div class="form-group row">
  <label class="col-12 col-sm-2 col-form-label text-sm-right"> Period of Employment From </label>
  <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
      <input size="16" type="text" value="{{ date('Y-m-d',time()) }}" name="period_from"
             class="form-control form-control-sm"
             placeholder="Date Now"
      >
      <div class="input-group-append">
        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
      </div>
    </div>
  </div>
  {{ Form::label('employment_status', 'Action', ['class'=>'col-12 col-sm-5 col-lg-3 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-3">
    {{ Form::select('action_employee_status', config('params.employee_status'), @$attestation->action_employee_status,[
            'class' => 'form-control form-control-xs',
            'placeholder' => 'Employment Status',
        ])
    }}
  </div>
</div>

<div class="form-group row">
  <label class="col-12 col-sm-2 col-form-label text-sm-right"> To </label>
  <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
      <input size="16" type="text" value="{{ $attestation->period_to }}" name="period_to"
             class="form-control form-control-sm"
             placeholder="Date Now"
      >
      <div class="input-group-append">
        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
      </div>
    </div>
  </div>
  {{ Form::label('employment_status', 'Date of Action', ['class'=>'col-12 col-sm-5 col-lg-3 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-3">
    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
      <input size="16" type="text" value="{{ $attestation->date_action }}" name="date_action"
             class="form-control form-control-sm"
             placeholder="Date Now"
      >
      <div class="input-group-append">
        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
      </div>
    </div>
  </div>
</div>

<div class="form-group row">
  {{ Form::label('appointment_nature', 'Nature of Appointment', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-4">
    {{ Form::text('nature_of_appointment', config('params.nature_of_appointment.'.@$appointmentform->nature_of_appointment), [
            'class' => 'form-control form-control-sm',
            'placeholder' => 'Nature of Appointment',
            'readOnly' => 'true'
        ])
    }}
  </div>
  {{ Form::label('employment_status', 'Date of Release', ['class'=>'col-12 col-sm-5 col-lg-2 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-3">
    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
      <input size="16" type="text" value="{{ $attestation->date_release }}" name="date_release"
             class="form-control form-control-sm"
             placeholder="Date Now"
      >
      <div class="input-group-append">
        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
      </div>
    </div>
  </div>
</div>

<div class="form-group row">
  <label class="col-12 col-sm-2 col-form-label text-sm-right"> Date of Issuance </label>
  <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
      <input size="16" type="text" value="{{ $attestation->date_issuance }}" name="date_issuance"
             class="form-control form-control-sm"
             placeholder="Date Now"
      >
      <div class="input-group-append">
        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12 col-sm-4 col-form-label text-sm-right"><strong>PUBLICATION</strong></div>
  <div class="col-12 offset-sm-3 col-sm-5 col-lg-3 col-form-label text-sm-right">Agency Receiving Offer</div>
</div>

<div class="form-group row">
  <label class="col-12 col-sm-2 col-form-label text-sm-right"> Publication Date From </label>
  <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
      <input size="16" type="text" value="{{ $attestation->publication_from }}" name="publication_from"
             class="form-control form-control-sm"
             placeholder="Date Now"
      >
      <div class="input-group-append">
        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
      </div>
    </div>
  </div>
  <div class="offset-sm-3 col-sm-8 offset-lg-3 col-lg-3">
    <input size="16" type="text" value="{{ $attestation->agency_receiving_offer}}" name="agency_receiving_offer"
           class="form-control form-control-sm"
    >
  </div>
</div>

<div class="form-group row">
  <label class="col-12 col-sm-2 col-form-label text-sm-right"> To </label>
  <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
      <input size="16" type="text" value="{{ $attestation->publication_to}}" name="publication_to"
             class="form-control form-control-sm"
             placeholder="Date Now"
      >
      <div class="input-group-append">
        <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
      </div>
    </div>
  </div>
</div>

<div class="form-group row">
  {{ Form::label('publication_mode', 'Mode of Publication', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-3">
    {{ Form::text('publication',config('params.publication.'.$attestation->applicant->publication), [
            'class' => 'form-control form-control-sm',
            'placeholder' => 'Mode of Publication',
            'readOnly' => 'true'
        ])
    }}
  </div>
</div>

<div class="form-group row text-right">
  <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
    {{ Form::submit('Save', ['id' => 'attestation-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>

{!! Form::close() !!}

@section('scripts')
  <!-- JS Libraries -->
  <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
          type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.formElements();
    });
  </script>
@endsection
