@section('css')
      <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/select2/css/select2.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}">
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'store-appointment-form']) !!}

<div class="form-group row">
    {{ Form::label('applicant_name', 'Applicant Name', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
    {{ Form::label('applicant_name', $applicant->getFullName() ,['class'=>'col-12 col-sm-2 col-form-label font-weight-bold']) }}
    </div>

    <div class="col-7 text-center">
        {{ Form::label('','PUBLICATION',['class' => 'col-12 col-form-label font-weight-bold'])}}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('position', 'Position', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('position', $applicant->job->plantilla_item->position->Name, [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
            ])
        }}
    </div>

    <label class="col-12 col-sm-3 col-form-label text-sm-right">Publication Date From </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="publication_date_from"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{ @$issued->publication_date_from}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="form-group row {{ $errors->has('employee_status') ? 'has-error' : ''}}">
    {{ Form::label('status', 'Employee Status', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::select('employee_status', config('params.employee_status'), @$issued->employee_status,[
                'class' => 'form-control form-control-xs',
                'placeholder' => 'Select employee status',
                'required' => 'true'
            ])
        }}

        {!! $errors->first('employee_status', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>

    <label class="col-12 col-sm-3 col-form-label text-sm-right">To </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="publication_date_to"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{ @$issued->publication_date_to}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('office', 'Office/Department/Unit', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('office', @$applicant->job->plantilla_item->office->Name, [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
            ])
        }}
    </div>

    {{ Form::label('', 'Mode of Publication', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('publication',config('params.publication.'.$applicant->publication) , [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('item_no', 'Item No.', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('item_no', $applicant->job->plantilla_item->Name, [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
            ])
        }}
    </div>

    <div class="col-7 text-center">
        {{ Form::label('','CERTIFICATION',['class' => 'col-12 col-form-label font-weight-bold'])}}
    </div>
</div>

<div class="form-group row">

    {{ Form::label('', 'Job Grade.', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('', $applicant->job->plantilla_item->salary_grade->Name, [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
            ])
        }}
    </div>


     {{ Form::label('', 'Highest Ranking HRMO', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        {{ Form::text('hrmo', @$issued->hrmo, [
                'class' => 'form-control form-control-sm',
                'required' => true,
            ])
        }}
    </div>

</div>

<div class="form-group row">

    {{ Form::label('', 'Monthly Rate', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('monthlyrate', $applicant->job->monthly_basic_salary, [
                'class' => 'form-control form-control-sm',
                'disabled' => true,
            ])
        }}
    </div>

    <label class="col-12 col-sm-3 col-form-label text-sm-right">Date Sign </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="hrmo_date_sign"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{ @$issued->hrmo_date_sign}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>


</div>

<div class="form-group row">

    <label class="col-12 col-sm-2 col-form-label text-sm-right">Date Issued </label>
   <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="date_issued"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{ @$issued->date_issued}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>

    {{ Form::label('','Chairperson, HRMPSB/Placement', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::text('chairperson', @$issued->chairperson,['class' => 'form-control form-control-sm'])
        }}
    </div>

</div>


<div class="form-group row">
    <label class="col-12 col-sm-2 col-form-label text-sm-right">Period of Employment</label>
    <div class="col-2">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="period_of_employment_from"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{ @$issued->period_of_employment_from}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
    <div class="col-2">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="period_of_employment_to"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{ @$issued->period_of_employment_to}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>

    <label class="col-12 col-sm-2 col-form-label text-sm-right">Date Sign </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" name="chairperson_date_sign"
                   class="form-control form-control-sm"
                   placeholder="Select date"
                   value="{{ @$issued->chairperson_date_sign}}">
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('', 'Nature of Appointment', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-3">
        {{ Form::select('nature_of_appointment', config('params.nature_of_appointment'), @$issued->nature_of_appointment,[
                'class' => 'form-control form-control-xs',
                'placeholder' => 'Select nature of appointment',
                'required' => 'true'
            ])
        }}
    </div>


</div>


<div class="form-group row text-right">
  <div class="col col-sm-12 ">
    <input type="hidden" name="issued_id" value="{{@$issued->id}}">
    <input type="hidden" name="applicant_id" value="{{$applicant->id}}">
    {{ Form::submit('Save', ['id' => 'appointment-submit', 'class'=>'btn btn-success btn-space']) }}
    {{ Form::reset('Cancel', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
  </div>
</div>
{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
        $('#store-appointment-form').parsley(); //frontend validation
      });
    </script>
@endsection
