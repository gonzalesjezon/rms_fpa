@section('css')
<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection

<div class="row">
    <div class="col-6">
		{!! Form::open(['action' => $actionPosition, 'method' => 'GET', 'id' => 'get-form']) !!}
        <div class="form-group row {{ $errors->has('job_id') ? 'has-error' : ''}}">
            {{ Form::label('job_id', 'Position Title', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select name="job_id" id="job_id" class="form-control form-control-xs">
				      <option value="0">Select position</option>
				      @foreach($jobs as $job)
				      <option value="{{$job->id}}" {{ ($job->id == @$currentJob->id) ? 'selected' : '' }}>{{$job->plantilla_item->position->Name}}</option>
				      @endforeach
			    </select>
                {!! $errors->first('job_id', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
		{!! Form::close() !!}
    </div>
</div>

{!! Form::open(['action' => $action, 'method' => $method, 'id'=>'interview-form']) !!}
<input type="hidden" name="interview_id" value="{{ @$interview->id}}">
<input type="hidden" name="job_id" id="put_job_id">
<div class="row">
	<div class="col-6">
		<div class="form-group row">
			{{ Form::label('', 'Applicant', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				<select name="applicant_id" class="form-control form-control-xs" id="applicant_id" required="true">
					<option value="0">Select applicant</option>
					@if(@$selected)
						@foreach($selected as $select)
						<option value="{{$select->applicant_id}}" data-job_id="{{$select->applicant->job_id}}"  data-email={{$select->applicant->email_address}} {{ ($select->applicant_id == @$interview->applicant_id) ? 'selected' : '' }}>{{$select->applicant->getFullName()}}</option>
						@endforeach
					@endif
				</select>
				{!! $errors->first('applicant_id', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
			</div>
		</div>

		<div class="form-group row">
		    <label class="col-12 col-sm-3 col-form-label text-sm-right"> Interview Date </label>
		    <div class="col-12 col-sm-7 col-md-5 col-lg-6">
		        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
		            <input size="16" type="text" value="{{@$interview->interview_date}}" name="interview_date"
		                   class="form-control form-control-sm" >
		            <div class="input-group-append">
		                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Time', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::text('interview_time', @$interview->interview_time, [
					'class' => 'form-control form-control-sm',
					'required' => true
				]) }}
			</div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Location', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::text('interview_location', @$interview->interview_location, [
					'class' => 'form-control form-control-sm',
					'required' => true
				]) }}
			</div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Email Address', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::text('email', @$interview->applicant->email_address, [
					'id' => 'email',
					'class' => 'form-control form-control-sm',
					'readonly' => true
				]) }}
			</div>
		</div>
	</div>

	<div class="col-6">
		<div class="form-group row">
			<label class="col-12 col-sm-3 col-form-label text-sm-right font-weight-bold"> If Reschedule </label>
		</div>

		<div class="form-group row">
		    <label class="col-12 col-sm-3 col-form-label text-sm-right"> Date </label>
		    <div class="col-12 col-sm-7 col-md-5 col-lg-6">
		        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
		            <input size="16" type="text" value="{{ @$examination->resched_interview_date }}" name="resched_interview_date"
		                   class="form-control form-control-sm">
		            <div class="input-group-append">
		                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Time', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::text('resched_interview_time', @$interview->resched_interview_time, [
					'class' => 'form-control form-control-sm',
				]) }}
			</div>
		</div>

		<hr>

		<div class="form-group row">
			{{ Form::label('','Interview Status', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::select('interview_status', config('params.interview_status'), @$interview->interview_status,[
					'class' => 'form-control form-control-xs',
					'placeholder' => 'Select interview status'
				]) }}
			</div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Notify Applicant', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
              <div class="switch-button switch-button-success switch-button-yesno">
              	 @if(@$interview->notify == 1)
                  <input type="checkbox" name="notify" id="notify" checked="true"><span>
                 @else
                 <input type="checkbox" name="notify" id="notify"><span>
                 @endif
                 <label for="notify"></label></span>
              </div>
          </div>
		</div>
	</div>
</div>

<hr>

<div class="row form-group">
	<div class="col-12">
		{{ Form::label('','Personnel Selection Board:', ['class' => 'col-form-label text-sm-right font-weight-bold']) }}
	</div>
</div>

<div class="row">
	<div class="col-2">
		{{ Form::text('psb_chairperson', @$interview->psb_chairperson, [
			'class' => 'form-control form-control-sm',

		]) }}
	</div>

	<div class="col-2">
		{{ Form::text('psb_secretariat', @$interview->psb_secretariat, [
			'class' => 'form-control form-control-sm',

		]) }}
	</div>

	<div class="col-2">
		{{ Form::text('psb_member', @$interview->psb_member, [
			'class' => 'form-control form-control-sm',

		]) }}
	</div>

	<div class="col-2">
		{{ Form::text('psm_sweap_rep', @$interview->psm_sweap_rep, [
			'class' => 'form-control form-control-sm',

		]) }}
	</div>

	<div class="col-2">
		{{ Form::text('psb_end_user', @$interview->psb_end_user, [
			'class' => 'form-control form-control-sm',

		]) }}
	</div>
</div>
<div class="row text-center ">
	<div class="col-2">
		{{ Form::label('','PSB Chairperson', ['class' => 'col-form-label text-sm-right font-weight-bold']) }}
	</div>

	<div class="col-2">
		{{ Form::label('','PSB Secretariat', ['class' => 'col-form-label text-sm-right font-weight-bold']) }}
	</div>

	<div class="col-2">
		{{ Form::label('','PSB Member', ['class' => 'col-form-label text-sm-right font-weight-bold']) }}
	</div>

	<div class="col-2">
		{{ Form::label('','PSB Sweap Rep', ['class' => 'col-form-label text-sm-right font-weight-bold']) }}
	</div>

	<div class="col-2">
		{{ Form::label('','PSB End-User', ['class' => 'col-form-label text-sm-right font-weight-bold']) }}
	</div>
</div>

<div class="form-group row text-right">
    <div class="col col-sm-12 ">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
{!! Form::close() !!}

@section('scripts')
    @include('interviews._form-script')
@endsection
